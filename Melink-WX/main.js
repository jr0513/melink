import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

let baseUrl = "http://172.168.1.102:9898/melink"
Vue.prototype.url = {
	register: baseUrl + "/register",
	login: baseUrl + "/login",
	checkin: baseUrl + "/checkin/checkin",
	createFaceModel: baseUrl + "/face/creatFaceMoldel",
	validCanCheckIn: baseUrl + "/checkin/validCanCheckin",
	searchTodayChekin: baseUrl + "/checkin/searchTodayChekin",
	searchUserSummary: baseUrl + "/user/searchUserSummary",
	searchMonthCheckin: baseUrl + "/checkin/searchMonthCheckin",
	refreshMessage: baseUrl + "/message/refreshMessage",
	searchMessageByPage: baseUrl + "/message/searchMessageByPage"
}

Vue.prototype.checkPermission=function(perms){
	let permission = uni.getStorageSync("permissions")
	let result = false
	for (let one of perms) {
		if(permission.indexOf(one) != -1){
			result = true
			break
		}
	}
	return result;
}

Vue.prototype.ajax = function(url,method,data,fun){
	uni.request({
		"url":url,
		"method":method,
		"data":data,
		"header":{
			token:uni.getStorageSync("token")
		},
		success:function(resp){
			if(resp.statusCode==401){
		 		uni.redirectTo({
					url:"/pages/login/login.vue"
				})
			}else if(resp.statusCode==200&&resp.data.status==200){
				let data = resp.data.data
				//console.log(resp.data)
				//console.log(resp.data.status)
				console.log(data)
				if(data != null){
					if(data.hasOwnProperty("token")){
						let token = data.token
						console.log(token)
						uni.setStorageSync("token",token)
					}
					fun(resp)
				}
				fun(resp)
			}else if(resp.statusCode==200&&resp.data.status==500){
				uni.showToast({
					icon:"none",
					title:resp.data.msg
				})
			}else if(resp.statusCode == 500){
				uni.showToast({
					icon:"none",
					title:"服务器出问题"
				})
			}else{
				uni.showToast({
					icon:"none",
					title:resp.data.msg
				})
			}
		}
	})
}

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()

