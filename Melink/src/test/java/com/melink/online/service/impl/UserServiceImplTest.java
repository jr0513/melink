package com.melink.online.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.melink.online.MelinkApplication;
import com.melink.online.entity.User;
import com.melink.online.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = MelinkApplication.class)
class UserServiceImplTest {

    @Autowired
    UserMapper userMapper;

    @Test
    void test1() {
        Long aLong = userMapper.selectCount(new QueryWrapper<User>().lambda().eq(User::getRoot, 1));
        System.out.println(aLong);
    }
}