package com.melink.online;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.melink.online.entity.*;
import com.melink.online.entity.vo.MeetingVO;
import com.melink.online.mapper.DeptMapper;
import com.melink.online.mapper.MeetingMapper;
import com.melink.online.mapper.UserMapper;
import com.melink.online.service.MeetingService;
import com.melink.online.service.MemberService;
import com.melink.online.service.MessageService;
import com.melink.online.service.UserService;
import org.apache.ibatis.annotations.Select;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class MelinkApplicationTests {

    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;
    @Autowired
    MeetingMapper meetingMapper;
    @Autowired
    DeptMapper deptMapper;
    @Autowired
    MemberService memberService;
    @Autowired
    private MeetingService meetingService;
    @Autowired
    UserMapper userMapper;

    @Test
    void contextLoads() {
        for (int i = 0; i <+ 100; i++) {
            MessageEntity message = new MessageEntity();
            message.setUuid(IdUtil.simpleUUID());
            message.setSenderId("0");
            message.setSenderName("系统消息");
            message.setMsg("这是第" + i + "条测试消息");
            message.setSendTime(new Date());
            String id = messageService.insertMessage(message);
            MessageRefEntity ref = new MessageRefEntity();
            ref.setMessageId(id);
            ref.setReceiverId("47e916ff89d256f6bbd31749a7ab08c4");
            ref.setLastFlag(true);
            ref.setReadFlag(false);
            messageService.insertRef(ref);
        }
    }

    @Test
    void createMeetingData(){
        Meeting meeting = new Meeting();
        meeting.setTitle("测试会议插入返回值");
        meeting.setCreatorId("47e916ff89d256f6bbd31749a7ab08c4");
        meeting.setPlace("线上会议");
        meeting.setStart("08:30");
        meeting.setEnd("10:30");
        meeting.setType(1);
        meeting.setDescribes("会议研讨Melink上线测试");
        meeting.setStatus(3);
        meetingMapper.insert(meeting);
        Member member = new Member();
        member.setMid(meeting.getId());
        member.setUid(meeting.getCreatorId());
        memberService.save(member);
    }

    @Test
    void searchMyMeetingListByPage(){
        LambdaQueryWrapper<User> userWrapper = new LambdaQueryWrapper<>();
        userWrapper.eq(User::getId,"47e916ff89d256f6bbd31749a7ab08c4").eq(User::getStatus,1);
        User user = userService.getOne(userWrapper);

        LambdaQueryWrapper<Meeting> meetingWrapper = new LambdaQueryWrapper<>();
        meetingWrapper.eq(Meeting::getCreatorId,user.getId()).orderByDesc(true,Meeting::getMeetingDate,Meeting::getStart,Meeting::getId);
        //List<Meeting> meetings = meetingMapper.selectList(meetingWrapper);
        Page<Meeting> page = new Page(2, 20);
        Page<Meeting> meetingPage = meetingMapper.selectPage(page, meetingWrapper);
        List<Meeting> records = meetingPage.getRecords();
        List<MeetingVO> meetingVOS = new ArrayList<>();
        System.out.println("============================================================");
        for (Meeting meeting : records) {
            MeetingVO meetingVO = new MeetingVO();
            meetingVO.setId(meeting.getId());
            meetingVO.setUuid(meeting.getUuid());
            meetingVO.setTitle(meeting.getTitle());
            meetingVO.setName(user.getName());
            meetingVO.setMeetingDate(DateUtil.format(DateUtil.parse(meeting.getMeetingDate()),"yyyy年MM月dd日"));
            meetingVO.setPlace(meeting.getPlace());
            meetingVO.setStart(DateUtil.format(DateUtil.parse(meeting.getStart()),"HH:mm"));
            meetingVO.setEnd(DateUtil.format(DateUtil.parse(meeting.getEnd()),"HH:mm"));
            meetingVO.setType(meeting.getType());
            meetingVO.setDescribes(meeting.getDescribes());
            meetingVO.setStatus(meeting.getStatus());
            meetingVO.setPhoto(user.getPhoto());
            meetingVO.setHour(String.valueOf(DateUtil.between(DateUtil.parse(meeting.getStart()), DateUtil.parse(meeting.getEnd()), DateUnit.HOUR)));
            meetingVOS.add(meetingVO);
            System.out.println(meetingVO);
        }
        System.out.println("============================================================");
    }

    @Test
    void search(){
        List<Map<String, Object>> members = deptMapper.searchDeptMembers(null);
        for (Map<String, Object> member : members) {
            System.out.println(member);
        }
    }

    @Test
    void find(){
        List<String> list = new ArrayList<>();
        list.add("091b55c6bdb24dd3bc510f194cb03a12");
        list.add("1ac30f6869e9461aa7d76bc5a63678ff");
        list.add("cd8de128a84b4991855a4929d69e511b");
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("status", 1).in("id", list);
        List<User> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);

    }
}
