package com.melink.online.generator;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

public class GeneratorMain {

    private static final String url = "jdbc:mysql://172.168.1.101:3306/melink?useSSL=false&serverTimezone=Hongkong&characterEncoding=utf-8";
    private static final String username = "root";
    private static final String password = "root";

    public static void main(String[] args) {

        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> {
                    builder.author("周迪") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("D:\\Project\\Melink\\src\\main\\java"); // 指定输出目录
                            //.outputDir("D:\\"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.melink") // 设置父包名
                            .moduleName("online") // 设置父包模块名
                            .entity("entity")
                            .service("service")
                            .serviceImpl("service.impl")
                            .mapper("mapper")
                            .xml("mapper")
                            .controller("controller")
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\Project\\Melink\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                            //.pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D:\\mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addTablePrefix("tb_","sys_")
                            .addInclude("tb_member")
                            //.addInclude("tb_permission_role")
                            //.addInclude("sys_config")
                            //.addInclude("tb_action")// 设置需要生成的表名
                            //.addInclude("tb_checkin")// 设置需要生成的表名
                            //.addInclude("tb_city")// 设置需要生成的表名
                            //.addInclude("tb_dept")// 设置需要生成的表名
                            //.addInclude("tb_face_model")// 设置需要生成的表名
                            //.addInclude("tb_holidays")// 设置需要生成的表名
                            //.addInclude("tb_meeting")// 设置需要生成的表名
                            //.addInclude("tb_module")// 设置需要生成的表名
                            //.addInclude("tb_permission")// 设置需要生成的表名
                            //.addInclude("tb_role")// 设置需要生成的表名
                            //.addInclude("tb_user")// 设置需要生成的表名
                            //.addInclude("tb_workday")// 设置需要生成的表名
                            //.addInclude("menu_role")// 设置需要生成的表名
                            //.addInclude("msgcontent")// 设置需要生成的表名
                            //.addInclude("nation")// 设置需要生成的表名
                            //.addInclude("oplog")// 设置需要生成的表名
                            //.addInclude("politicsstatus")// 设置需要生成的表名
                            //.addInclude("position")// 设置需要生成的表名
                            //.addInclude("role")// 设置需要生成的表名
                            //.addInclude("salary")// 设置需要生成的表名
                            //.addInclude("sysmsg")// 设置需要生成的表名
                            .entityBuilder().formatFileName("%s").enableLombok().enableTableFieldAnnotation()
                            .controllerBuilder().formatFileName("%sController")
                            .serviceBuilder().formatServiceFileName("%sService")
                            .mapperBuilder().superClass(BaseMapper.class)
                            .enableMapperAnnotation()
                            .enableBaseResultMap()
                            .enableBaseColumnList()
                            .formatMapperFileName("%sMapper")
                            .formatXmlFileName("%s")
                    ; // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }

}
