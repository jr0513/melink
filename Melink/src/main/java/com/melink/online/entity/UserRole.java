package com.melink.online.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 周迪
 * @since 2022-01-18
 */
@Getter
@Setter
@TableName("tb_user_role")
@ApiModel(value = "UserRole对象", description = "")
public class UserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键ID")
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("角色ID")
    @TableField("rid")
    private Integer rid;

    @ApiModelProperty("用户ID")
    @TableField("uid")
    private String uid;


}
