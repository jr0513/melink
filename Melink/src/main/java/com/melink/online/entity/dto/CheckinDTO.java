package com.melink.online.entity.dto;

import lombok.Data;

@Data
public class CheckinDTO {
    private String userId;
    private String startTime;
    private String endTime;

}
