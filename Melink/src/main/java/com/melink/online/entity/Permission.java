package com.melink.online.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

/**
 * <p>
 * 
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Getter
@Setter
@TableName("tb_permission")
@ApiModel(value = "PermissionEntity对象", description = "")
public class Permission implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("权限")
    @TableField("permission_name")
    private String permissionName;

    @ApiModelProperty("模块ID")
    @TableField("module_id")
    private Integer moduleId;

    @ApiModelProperty("行为ID")
    @TableField("action_id")
    private Integer actionId;

}
