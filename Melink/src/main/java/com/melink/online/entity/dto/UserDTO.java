package com.melink.online.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class UserDTO {
    private Integer id;
    private String openId;
    private String nickname;
    private String photo;
    private String name;
    private String sex;
    private String tel;
    private String email;
    private LocalDate hiredate;
    @NotBlank
    private Integer root;
    private Integer deptId;
    @NotBlank
    private Integer status;
    private LocalDateTime createTime;
    private String password;

}
