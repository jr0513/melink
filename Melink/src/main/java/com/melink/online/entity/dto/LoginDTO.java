package com.melink.online.entity.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel("登录参数")
public class LoginDTO {

    @NotBlank(message = "临时授权码不能为空")
    private String code;
}
