package com.melink.online.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel
public class TestForm {

    @ApiModelProperty("名称")
    //@NotBlank
    //@Pattern(regexp = "^[\\u4e00-\\u9fa5]{2,15}$")
    private String name;
}
