package com.melink.online.entity.form;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Data
@ApiModel
@ToString
public class SearchMembersForm {
    @NotNull
    private String[] ids;
}
