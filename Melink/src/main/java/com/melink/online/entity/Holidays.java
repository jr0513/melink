package com.melink.online.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 节假日表
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Getter
@Setter
@TableName("tb_holidays")
@ApiModel(value = "HolidaysEntity对象", description = "节假日表")
public class Holidays implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("日期")
    @TableField("date")
    private LocalDate date;


}
