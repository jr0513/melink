package com.melink.online.entity.dto;

import lombok.Data;

@Data
public class WxLoginDTO {
    private String openid;
    private String session_key;
}
