package com.melink.online.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 会议表
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@Getter
@Setter
@TableName("tb_meeting")
@ApiModel(value = "Meeting对象", description = "会议表")
public class Meeting implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("UUID")
    @TableField(value = "uuid")
    private String uuid;

    @ApiModelProperty("会议题目")
    @TableField("title")
    private String title;

    @ApiModelProperty("创建人ID")
    @TableField("creator_id")
    private String creatorId;

    @ApiModelProperty("日期")
    @TableField(value = "date",fill = FieldFill.INSERT)
    private String meetingDate;

    @ApiModelProperty("开会地点")
    @TableField("place")
    private String place;

    @ApiModelProperty("开始时间")
    @TableField("start")
    private String start;

    @ApiModelProperty("结束时间")
    @TableField("end")
    private String end;

    @ApiModelProperty("会议类型（1在线会议，2线下会议）")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("工作流实例ID")
    @TableField(value = "instance_id",fill = FieldFill.INSERT)
    private String instanceId;

    @ApiModelProperty("状态（1未开始，2进行中，3已结束）")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("会议内容")
    @TableField("describes")
    private String describes;


}
