package com.melink.online.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 签到表
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Getter
@Setter
@TableName("tb_checkin")
@ApiModel(value = "CheckinEntity对象", description = "签到表")
public class Checkin implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @TableId(value = "id", type = IdType.ASSIGN_UUID)
    private String id;

    @ApiModelProperty("用户ID")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty("签到地址")
    @TableField("address")
    private String address;

    @ApiModelProperty("国家")
    @TableField("country")
    private String country;

    @ApiModelProperty("省份")
    @TableField("province")
    private String province;

    @ApiModelProperty("城市")
    @TableField("city")
    private String city;

    @ApiModelProperty("区划")
    @TableField("district")
    private String district;

    @ApiModelProperty("考勤结果")
    @TableField(value = "status")
    private Integer status;

    @ApiModelProperty("风险等级")
    @TableField("risk")
    private Integer risk;

    @ApiModelProperty("签到日期")
    @TableField(value = "date",fill = FieldFill.INSERT)
    private LocalDate date;

    @ApiModelProperty("签到时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;


}
