package com.melink.online.entity.vo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class MeetingVO {
    private Integer id;
    private String uuid;
    private String title;
    private String name;
    private String meetingDate;
    private String place;
    private String start;
    private String end;
    private Integer type;
    private String describes;
    private Integer status;
    private String photo;
    private String hour;
}
