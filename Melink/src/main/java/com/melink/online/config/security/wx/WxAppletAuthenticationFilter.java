package com.melink.online.config.security.wx;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.melink.online.common.utils.JwtUtils;
import com.melink.online.entity.dto.WxLoginDTO;
import com.melink.online.exception.MelinkException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Slf4j
public class WxAppletAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    @Value("${wx.app-id}")
    private String appid;

    @Value("${wx.app-secret}")
    private String appsrcret;

    private String wxAuthUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RestTemplate restTemplate;

    public WxAppletAuthenticationFilter() {
        super(new AntPathRequestMatcher("/login","POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        Map map = objectMapper.readValue(request.getInputStream(), Map.class);
        String code = (String) map.get("code");
        if(StringUtils.isBlank(code)){
            log.error("临时授权字符串为空");
            throw new MelinkException("临时授权字符串为空");
        }
        String url = String.format(wxAuthUrl, appid, appsrcret, code);
        log.info("微信认证URL:{}",url);
        WxLoginDTO loginDTO = restTemplate.getForObject(url, WxLoginDTO.class);
        if (loginDTO.getOpenid() == null){
            throw new MelinkException("微信认证失败");
        }
        //
        return this.getAuthenticationManager().authenticate(new WxAppletAuthenticationToken(loginDTO.getOpenid(),loginDTO.getSession_key()));
    }
}
