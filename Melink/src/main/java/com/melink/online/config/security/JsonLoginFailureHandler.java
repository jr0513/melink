package com.melink.online.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.melink.online.common.utils.RespBean;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class JsonLoginFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RespBean respBean = RespBean.fail("登陆失败");
        if (e instanceof UsernameNotFoundException || e instanceof AuthenticationException){
            respBean.setMsg("用户不存在");
            String value = new ObjectMapper().writeValueAsString(respBean);
            out.write(value);
            out.flush();
            out.close();
        }
        //if (e instanceof LockedException){
        //    respBean.setMsg("账户已被锁定");
        //}else if (e instanceof CredentialsExpiredException){
        //    respBean.setMsg("密码已过期");
        //}else if (e instanceof AccountExpiredException){
        //    respBean.setMsg("账户已过期");
        //}else if (e instanceof DisabledException){
        //    respBean.setMsg("账户已被禁用");
        //}else if (e instanceof BadCredentialsException){
        //    respBean.setMsg("用户名或者密码错误");
        //}

    }
}
