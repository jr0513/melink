package com.melink.online.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
public class SwaggerConfig {

//    /**
//     * 创建API应用
//     * apiInfo() 增加API相关信息
//     * 通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
//     * 本例采用指定扫描的包路径来定义指定要建立API的目录。
//     */
//    @Bean
//    public Docket createRestApi() {
////        return createParameterDocket();
//        return createSecurityDocket();
//    }
//
//    /**
//     * 为 Swagger 的每一个接口添加 Header 头参数
//     */
//    private Docket createParameterDocket() {
//        ParameterBuilder builder = new ParameterBuilder();
//        builder.name("Authorization").description("认证token")
//                .modelRef(new ModelRef("string"))
//                .parameterType("header")
//                .required(false)
//                .build();
//
//        return new Docket(DocumentationType.SWAGGER_2)
//                .apiInfo(apiInfo())
//                .select()
//                .apis(RequestHandlerSelectors.any())
////                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
//                .paths(PathSelectors.any())
//                .build()
//                .globalOperationParameters(Collections.singletonList(builder.build()));
//    }
//
//    /**
//     * 为 Swagger 文档上下文添加 Header 头参数
//     */
//    private Docket createSecurityDocket() {
//        return new Docket(DocumentationType.SWAGGER_2).
//                useDefaultResponseMessages(false)
//                .select()
//                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
//                .build()
//                .securitySchemes(Collections.singletonList(securityScheme()))
//                .securityContexts(Collections.singletonList(securityContext()));
//    }
//
//    private SecurityScheme securityScheme() {
//        return new ApiKey("token", "token", "header");
//    }
//
//    private SecurityContext securityContext() {
//        return SecurityContext.builder()
//                .securityReferences(defaultAuth())
//                //.forPaths(PathSelectors.regex("^(?!auth).*$"))
//                .build();
//    }
//
//    private List<SecurityReference> defaultAuth() {
//        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
//        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//        authorizationScopes[0] = authorizationScope;
//        return Collections.singletonList(
//                new SecurityReference("Authorization", authorizationScopes));
//    }
//
//    /**
//     * 创建该API的基本信息（这些基本信息会展现在文档页面中）
//     * 访问地址：http://项目实际地址/swagger-ui.html
//     */
//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("Melink线上办公")
//                .version("1.0")
//                .contact(new Contact("周迪", "", ""))
//                .build();
//    }


    @Bean
    public Docket createRestApi() {

         //请求携带令牌
        ApiKey apiKey = new ApiKey("token", "token", "header");
        List<ApiKey> apiKeyList = new ArrayList<>();
        apiKeyList.add(apiKey);

        AuthorizationScope scope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] scopes = {scope};
        SecurityReference reference = new SecurityReference("token", scopes);
        List refList = new ArrayList();
        refList.add(reference);
        SecurityContext context = SecurityContext.builder().securityReferences(refList).build();
        List<SecurityContext> cxtList = new ArrayList();
        cxtList.add(context);

        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select().paths(PathSelectors.any()).apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)).build()
                .securityContexts(cxtList)
                .securitySchemes(apiKeyList)
                .apiInfo(new ApiInfoBuilder()
                        .title("Melink线上办公")
                        .description("Melink线上办公API")
                        .version("1.0")
                        //.contact(new Contact("周迪","https://gitee.com/jr0513","993802837@qq.com"))
                        .license("The Apache License")
                        .licenseUrl("http://www.baidu.com")
                        .build());
    }
    //@Bean
    //public Docket createDocket(){
    //    return new Docket(DocumentationType.SWAGGER_2)
    //            // 文档信息配置
    //            .apiInfo(apiInfo())
    //            // 扫描策略配置（扫描带有ApiOperation注解的方法;以及所有路径）
    //            .select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class)).paths(PathSelectors.any()).build()
    //            // securitySchemes与securityContexts作用为配置全局Authorization参数
    //            .securitySchemes(securitySchemes())
    //            .securityContexts(securityContexts());
    //}
    //
    //// 接口文档信息
    //public ApiInfo apiInfo(){
    //    return new ApiInfo(
    //            "Melink线上办公",
    //            "Melink线上办公API",
    //            "1.0",
    //            "https://gitee.com/jr0513",
    //            new Contact("周迪", "https://gitee.com/jr0513", "993802837@qq.com"),
    //            "Apache 2.0",
    //            "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());
    //}
    //
    //public List<ApiKey> securitySchemes(){
    //    ApiKey apiKey = new ApiKey("token", "token", "header");
    //    List<ApiKey> apiKeyList = new ArrayList<>();
    //    apiKeyList.add(apiKey);
    //    return apiKeyList;
    //}
    //
    //public List<SecurityContext> securityContexts(){
    //    AuthorizationScope[] scopes = {new AuthorizationScope("global", "accessEverything")};
    //    SecurityReference reference = new SecurityReference("token", scopes);
    //    List<SecurityReference> refList = new ArrayList<>();
    //    refList.add(reference);
    //    SecurityContext context = SecurityContext.builder().securityReferences(refList).build();
    //    List<SecurityContext> cxtList = new ArrayList<>();
    //    cxtList.add(context);
    //    return cxtList;
    //}
}

