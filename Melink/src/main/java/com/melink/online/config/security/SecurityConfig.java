package com.melink.online.config.security;

import com.melink.online.config.security.wx.JwtAuthenticationTokenFilter;
import com.melink.online.config.security.wx.WxAppletAuthenticationFilter;
import com.melink.online.config.security.wx.WxAppletAuthenticationManager;
import com.melink.online.config.security.wx.WxAuthenticationProvider;
import com.melink.online.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserServiceImpl userService;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/webjars/**",
                "/druid/**",
                "/app/**",
                "/swagger/**",
                "/v2/api-docs",
                "/swagger-ui.html",
                "/swagger-resources/**",
                "/captcha.jpg");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").anonymous()
                .antMatchers("/register").permitAll()
                .antMatchers("/error/**").permitAll()
                .anyRequest().authenticated()
                .and()
                    // 禁用CSRF
                    .csrf().disable()
                    // 禁用Session
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .exceptionHandling()
                        .authenticationEntryPoint(new JsonAuthenticationEntryPoint())
                        .accessDeniedHandler(new JsonAccessDeniedHandler())
                .and()
                // 使用WxAppletAuthenticationFilter替换默认的认证过滤器UsernamePasswordAuthenticationFilter
                .addFilterAt(wxAppletAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
                // 在WxAppletAuthenticationFilter前面添加用于验证jwt，识别用户是否登录的过滤器
                .addFilterBefore(jwtAuthenticationTokenFilter(), WxAppletAuthenticationFilter.class);
    }

    @Bean
    public JsonLoginSuccessHandler jsonLoginSuccessHandler() {
        return new JsonLoginSuccessHandler();
    }

    @Bean
    public JsonLoginFailureHandler jsonLoginFailureHandler(){
        return new JsonLoginFailureHandler();
    }

    @Autowired
    private WxAppletAuthenticationManager wxAppletAuthenticationManager;

    @Bean
    public WxAppletAuthenticationFilter wxAppletAuthenticationFilter(){
        WxAppletAuthenticationFilter wxAppletAuthenticationFilter = new WxAppletAuthenticationFilter();
        wxAppletAuthenticationFilter.setAuthenticationManager(wxAppletAuthenticationManager);
        wxAppletAuthenticationFilter.setAuthenticationSuccessHandler(jsonLoginSuccessHandler());
        wxAppletAuthenticationFilter.setAuthenticationFailureHandler(jsonLoginFailureHandler());
        return wxAppletAuthenticationFilter;
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() {
        return new JwtAuthenticationTokenFilter();
    }
}
