package com.melink.online.config.security.wx;

import com.melink.online.entity.Role;
import com.melink.online.entity.User;
import com.melink.online.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WxAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String openId = (String) authentication.getPrincipal();
        String sessionKey = (String) authentication.getCredentials();
        // 查询用户
        User user = userService.getUserByOpenId(openId);
        // 查询权限
        List<Role> authorities = userService.getAuthorities(user);
        if (user == null){
            throw new UsernameNotFoundException("用户未注册");
        }
        return new WxAppletAuthenticationToken(openId, sessionKey,authorities);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(WxAppletAuthenticationToken.class);
    }
}
