package com.melink.online.config.security.wx;

import com.melink.online.entity.Role;
import com.melink.online.entity.User;
import com.melink.online.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@Slf4j
public class WxAppletAuthenticationManager implements AuthenticationManager {

    @Autowired
    private WxAuthenticationProvider wxAuthenticationProvider;
    // 参考：https://www.cnblogs.com/zhengqing/p/11670991.html、https://blog.csdn.net/wamr_o/article/details/98858011

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        // 真正的认证交给 WxAuthenticationProvider 处理
        Authentication result = wxAuthenticationProvider.authenticate(authentication);
        if (Objects.nonNull(result)){
            return result;
        }
        throw new ProviderNotFoundException("认证失败");
    }
}
