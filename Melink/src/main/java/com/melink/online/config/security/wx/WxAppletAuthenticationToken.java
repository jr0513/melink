package com.melink.online.config.security.wx;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Getter
@Setter
public class WxAppletAuthenticationToken extends AbstractAuthenticationToken {

    private String openid;
    private String sessionKey;

    public WxAppletAuthenticationToken(String openid, String sessionKey) {
        super(null);
        this.openid = openid;
        this.sessionKey = sessionKey;
    }

    public WxAppletAuthenticationToken(String openid, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.openid = openid;
        super.setAuthenticated(true);
    }

    public WxAppletAuthenticationToken(String openid, String sessionKey, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.openid = openid;
        this.sessionKey = sessionKey;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return this.sessionKey;
    }

    @Override
    public Object getPrincipal() {
        return this.openid;
    }

    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "不能将此令牌设置为受信任的使用构造函数，该构造函数接受一个被授予的权限列表");
        }
        super.setAuthenticated(false);
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
    }
}
