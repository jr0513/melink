package com.melink.online.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RedisUtils;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.Role;
import com.melink.online.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class JsonLoginSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    UserService userService;

    @Autowired
    RedisUtils redisUtils;

    @Value("${melink.jwt.cache-expire}")
    private int cacheExpire;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
        // token与权限信息返回给前端
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String openid = (String) auth.getPrincipal();
        String id = userService.getUserIdByOpenId(openid);
        String token = jwtUtils.createToken(id);
        List<Role> collection = (List<Role>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        Set<String> authorities = new HashSet<>();
        for (Role role : collection) {
            System.out.println(role.getRoleName());
            authorities.add(role.getRoleName());
        }
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("token",token);
        map.put("permissions",authorities);
        RespBean ok = RespBean.ok("登录成功", map);
        String value = new ObjectMapper().writeValueAsString(ok);
        out.write(value);
        out.flush();
        out.close();
        // redis缓存token
        redisUtils.setExpireTokenOfDay(token, id, cacheExpire);
    }
}
