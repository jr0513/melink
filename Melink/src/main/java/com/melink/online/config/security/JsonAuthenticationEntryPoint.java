package com.melink.online.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.melink.online.common.utils.RespBean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class JsonAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        RespBean respBean = RespBean.fail("账号未登录，请登录后操作");
        String value = new ObjectMapper().writeValueAsString(respBean);
        out.write(value);
        out.flush();
        out.close();
    }
}
