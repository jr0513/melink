package com.melink.online.config.xss;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

    public XssHttpServletRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getParameter(String name) {
        String value =  super.getParameter(name);
        if (!StrUtil.hasEmpty(value)){
            value = HtmlUtil.filter(value);
        }
        return value;
    }

    @Override
    public String[] getParameterValues(String name) {
        String[] values = super.getParameterValues(name);
        if (ArrayUtil.isNotEmpty(values)){
            for (int i = 0; i < values.length; i++) {
                String filter = values[i];
                if (!StrUtil.hasEmpty(values[i])){
                    filter = HtmlUtil.filter(values[i]);
                }
                values[i] = filter;
            }
        }
        return values;
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        Map<String, String[]> parameterMap =  super.getParameterMap();
        LinkedHashMap<String, String[]> valuesMap = new LinkedHashMap<>();
        if (MapUtil.isNotEmpty(parameterMap)){
            for (String key : parameterMap.keySet()) {
                String[] values = parameterMap.get(key);
                if (ArrayUtil.isNotEmpty(values)){
                    for (int i = 0; i < values.length; i++) {
                        String filter = values[i];
                        if (!StrUtil.hasEmpty(values[i])){
                            filter = HtmlUtil.filter(values[i]);
                        }
                        values[i] = filter;
                    }
                }
                valuesMap.put(key,values);
            }
        }
        return valuesMap;
    }

    @Override
    public String getHeader(String name) {
        String value =  super.getHeader(name);
        if (!StrUtil.hasEmpty(value)){
            value = HtmlUtil.filter(value);
        }
        return value;
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        InputStream inputStream = super.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuffer buffer = new StringBuffer();
        String line = bufferedReader.readLine();
        while (!StrUtil.hasEmpty(line)){
            buffer.append(line);
            line = bufferedReader.readLine();
        }
        bufferedReader.close();
        inputStreamReader.close();
        inputStream.close();
        log.info("请求参数转义为JSON:{}",String.valueOf(JSONUtil.parseObj(buffer.toString())));
        Map<String, Object> map = JSONUtil.parseObj(buffer.toString());
        Map<String, Object> result = new LinkedHashMap<>();
        for (String key : map.keySet()) {
            Object value = map.get(key);
            if (value instanceof String && !StrUtil.hasEmpty(value.toString())){
                result.put(key,HtmlUtil.filter(value.toString()));
            }else {
                result.put(key,value);
            }
        }
        String jsonStr = JSONUtil.toJsonStr(result);
        ByteArrayInputStream stream = new ByteArrayInputStream(jsonStr.getBytes());
        return new ServletInputStream() {
            @Override
            public int read() throws IOException {
                return stream.read();
            }

            @Override
            public boolean isFinished() {
                return false;
            }

            @Override
            public boolean isReady() {
                return false;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }
        };
    }
}
