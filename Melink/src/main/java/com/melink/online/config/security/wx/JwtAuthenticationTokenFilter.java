package com.melink.online.config.security.wx;


import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RedisUtils;
import com.melink.online.entity.Role;
import com.melink.online.entity.User;
import com.melink.online.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    UserService userService;
    @Autowired
    RedisUtils redisUtils;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        // 获取token
        String token = request.getHeader("token");
        if (token != null && !token.isEmpty()){
            String userId = jwtUtils.getUserId(token);
            if (userId != null && !userId.isEmpty()){
                // 查询用户信息
                User user = userService.getById(userId);
                // 查询权限信息
                List<Role> authorities = userService.getAuthorities(user);
                // 信息设置到WxAppletAuthenticationToken中
                WxAppletAuthenticationToken authenticationToken = new WxAppletAuthenticationToken(user.getOpenId(), authorities);
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
            }else {
                // jwt验证过期后去redis中查询是否存在token,存在则更新token并认证成功，不存在则重新登录
                Object redisToken = redisUtils.getStringValue(userId);
                if (redisToken != null){
                    // 查询用户信息
                    User user = userService.getById(userId);
                    // 查询权限信息
                    List<Role> authorities = userService.getAuthorities(user);
                    // 信息设置到WxAppletAuthenticationToken中
                    WxAppletAuthenticationToken authenticationToken = new WxAppletAuthenticationToken(user.getOpenId(), authorities);
                    authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }
                filterChain.doFilter(request, response);
            }
        }
        filterChain.doFilter(request, response);
    }

}
