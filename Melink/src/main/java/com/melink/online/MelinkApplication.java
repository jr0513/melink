package com.melink.online;

import cn.hutool.core.util.StrUtil;
import com.melink.online.common.constant.SystemConstants;
import com.melink.online.entity.Config;
import com.melink.online.service.ConfigService;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;
import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

@SpringBootApplication
@MapperScan("com.melink.online.mapper")
@ServletComponentScan
@EnableTransactionManagement
@Slf4j
@EnableAsync
public class MelinkApplication {

    @Autowired
    ConfigService configService;

    @Autowired
    SystemConstants constants;

    @Value("${melink.image-folder}")
    private String imageFolder;

    public static void main(String[] args) {
        SpringApplication.run(MelinkApplication.class, args);
    }

    @PostConstruct
    public void init(){
        List<Config> list = configService.getList();
        list.forEach(one->{
            String key = one.getParamKey();
            key = StrUtil.toCamelCase(key);
            String value = one.getParamValue();
            try {
                Field field = constants.getClass().getDeclaredField(key);
                field.setAccessible(true);
                field.set(constants,value);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                log.error("初始化加载异常",e);
            }
        });
        new File(imageFolder).mkdir();
    }
}
