package com.melink.online.aop;

import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RedisUtils;
import com.melink.online.common.utils.RespBean;
import com.melink.online.service.UserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Aspect
public class RefreshTokenAspect {

    @Autowired
    UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RedisUtils redisUtils;

    @Value("${melink.jwt.cache-expire}")
    private int cacheExpire;

    @Pointcut("execution(public * com.melink.online.controller.*.*(..)) && !execution(public * com.melink.online.controller.LoginController.*(..))")
    public void refreshToken(){}

    @Around("refreshToken()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        RespBean proceed = (RespBean) joinPoint.proceed();
 //       String openId = GetOpenIdFromSecurity.getOpenId();
//        String userId = userService.getUserIdByOpenId(openId);
 //       String token = jwtUtils.createToken(userId);
        System.out.println("AOP（消息）："+proceed.getMsg());
        System.out.println("AOP（数据）："+proceed.getData());
        //Map map = new HashMap();
        //map.put("token",token);
        //proceed.setData(map);
        //redisUtils.setExpireTokenOfDay(token, userId, cacheExpire);
        return proceed;
    }

}
