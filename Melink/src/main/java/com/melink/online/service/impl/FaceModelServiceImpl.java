package com.melink.online.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.melink.online.common.utils.FaceEngineUtils;
import com.melink.online.entity.FaceModel;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.FaceModelMapper;
import com.melink.online.service.FaceModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class FaceModelServiceImpl extends ServiceImpl<FaceModelMapper, FaceModel> implements FaceModelService {

    @Autowired
    FaceModelMapper faceModelMapper;
    @Autowired
    FaceEngineUtils faceEngineUtils;

    /**
     * 查询用户人脸信息
     * @param userId 用户ID
     * @return 人脸信息
     */
    @Override
    public String searchFaceModel(String userId) {
        FaceModel faceModel = faceModelMapper.selectOne(new QueryWrapper<FaceModel>().lambda().eq(FaceModel::getUserId, userId));
        if (faceModel == null){
            return null;
        }
        String faceData = faceModel.getFaceModel();
        if (faceData == null){
            return null;
        }
        return faceData;
    }

    /**
     * 添加人脸数据
     * @param userId 用户ID
     * @param imagePath 图片路径
     */
    @Override
    public boolean insertFaceModel(String userId, String imagePath) {
        String face = faceEngineUtils.addFace(imagePath);
        FaceModel model = new FaceModel();
        model.setUserId(userId);
        model.setFaceModel(face);
        int insert = faceModelMapper.insert(model);
        if (insert != 1){
            return false;
        }
        return true;
    }

    /**
     * 删除人脸数据
     * @param userId 用户ID
     */
    @Override
    public int deleteFaceModel(String userId) {
        int delete = faceModelMapper.delete(new QueryWrapper<FaceModel>().lambda().eq(FaceModel::getUserId, userId));
        if (delete != 1){
            throw new MelinkException("人脸数据删除失败");
        }
        return delete;
    }
}
