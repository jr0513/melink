package com.melink.online.service;

import com.melink.online.entity.Module;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 模块资源表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface ModuleService extends IService<Module> {

}
