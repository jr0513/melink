package com.melink.online.service;

import com.melink.online.entity.Holidays;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 节假日表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface HolidaysService extends IService<Holidays> {

    boolean searchTodayIsHoliday();

}
