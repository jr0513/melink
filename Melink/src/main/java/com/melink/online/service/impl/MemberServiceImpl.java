package com.melink.online.service.impl;

import com.melink.online.entity.Member;
import com.melink.online.mapper.DeptMapper;
import com.melink.online.mapper.MemberMapper;
import com.melink.online.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {

}
