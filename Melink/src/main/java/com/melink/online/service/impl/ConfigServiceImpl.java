package com.melink.online.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.melink.online.entity.Config;
import com.melink.online.mapper.ConfigMapper;
import com.melink.online.service.ConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {

    @Autowired
    ConfigMapper configMapper;

    /**
     * 获取所有状态为1的Config
     * @return
     */
    @Override
    public List<Config> getList() {
        List<Config> configs = configMapper.selectList(new QueryWrapper<Config>().lambda().eq(Config::getStatus, 1));
        return configs;
    }
}
