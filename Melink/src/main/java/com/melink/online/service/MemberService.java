package com.melink.online.service;

import com.melink.online.entity.Member;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
public interface MemberService extends IService<Member> {


}
