package com.melink.online.service;

import com.melink.online.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 周迪
 * @since 2022-01-18
 */
public interface UserRoleService extends IService<UserRole> {

}
