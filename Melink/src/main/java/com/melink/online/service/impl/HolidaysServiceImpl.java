package com.melink.online.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.melink.online.entity.Holidays;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.HolidaysMapper;
import com.melink.online.service.HolidaysService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 节假日表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class HolidaysServiceImpl extends ServiceImpl<HolidaysMapper, Holidays> implements HolidaysService {

    @Autowired
    HolidaysMapper holidaysMapper;

    @Override
    public boolean searchTodayIsHoliday() {
        Holidays holidays = holidaysMapper.selectOne(new QueryWrapper<Holidays>().lambda().eq(Holidays::getDate, new Date()).last("limit 1"));
        if (holidays == null){
            return false;
        }
        return true;
    }
}
