package com.melink.online.service;

import com.melink.online.entity.Config;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface ConfigService extends IService<Config> {
    List<Config> getList();
}
