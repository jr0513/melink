package com.melink.online.service.impl;

import com.melink.online.entity.Role;
import com.melink.online.mapper.RoleMapper;
import com.melink.online.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

}
