package com.melink.online.service.impl;

import com.melink.online.entity.PermissionRole;
import com.melink.online.mapper.PermissionRoleMapper;
import com.melink.online.service.PermissionRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2022-01-18
 */
@Service
public class PermissionRoleServiceImpl extends ServiceImpl<PermissionRoleMapper, PermissionRole> implements PermissionRoleService {

}
