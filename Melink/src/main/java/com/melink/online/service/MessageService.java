package com.melink.online.service;

import com.melink.online.entity.MessageEntity;
import com.melink.online.entity.MessageRefEntity;

import java.util.List;
import java.util.Map;

public interface MessageService {
    String insertMessage(MessageEntity entity);
    List<Map> searchMessageByPage(String userId, long start, int length);
    Map searchMessageById(String id);
    String insertRef(MessageRefEntity entity);
    long searchUnreadCount(String userId);
    long searchLastCount(String userId);
    long updateUnreadMessage(String id);
    long deleteMessageRefById(String id);
    long deleteUserMessageRef(String userId);
}
