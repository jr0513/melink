package com.melink.online.service;

import com.melink.online.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface RoleService extends IService<Role> {

}
