package com.melink.online.service.impl;

import com.melink.online.entity.Dept;
import com.melink.online.mapper.DeptMapper;
import com.melink.online.service.DeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements DeptService {

}
