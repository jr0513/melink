package com.melink.online.service;

import com.melink.online.entity.Action;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 行为表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface ActionService extends IService<Action> {

}
