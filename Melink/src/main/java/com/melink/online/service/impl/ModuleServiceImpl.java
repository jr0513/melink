package com.melink.online.service.impl;

import com.melink.online.entity.Module;
import com.melink.online.mapper.ModuleMapper;
import com.melink.online.service.ModuleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模块资源表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class ModuleServiceImpl extends ServiceImpl<ModuleMapper, Module> implements ModuleService {

}
