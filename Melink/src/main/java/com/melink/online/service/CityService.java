package com.melink.online.service;

import com.melink.online.entity.City;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 疫情城市列表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface CityService extends IService<City> {
    String searchCode(String city);
}
