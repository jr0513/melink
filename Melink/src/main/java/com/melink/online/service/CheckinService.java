package com.melink.online.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.melink.online.entity.Checkin;
import com.melink.online.entity.dto.CheckinDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface CheckinService extends IService<Checkin> {

    String validCanCheckin(String userId);

    boolean haveCheckin(CheckinDTO checkinDTO);

    void insert(Checkin checkin);

    void checkin(Map param);

    Map searchTodayCheckin (String userId);

    long searchCheckinDays (String userId);

    List<Map> searchWeekCheckin (HashMap param);

    List<Map> searchMonthCheckin (HashMap param);

}
