package com.melink.online.service;

import com.melink.online.entity.Meeting;
import com.baomidou.mybatisplus.extension.service.IService;
import com.melink.online.entity.form.SearchMyMeetingListByPageForm;
import com.melink.online.entity.vo.MeetingVO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会议表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
public interface MeetingService extends IService<Meeting> {
    void insertingMeeting(Meeting entity);
    List<Map<String,Object>> searchMyMeetingListByPage(String userId, SearchMyMeetingListByPageForm form);
}
