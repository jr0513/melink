package com.melink.online.service.impl;

import com.melink.online.entity.UserRole;
import com.melink.online.mapper.UserRoleMapper;
import com.melink.online.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2022-01-18
 */
@Service
@Transactional
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

}
