package com.melink.online.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.melink.online.entity.*;
import com.melink.online.entity.dto.UserDTO;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.*;
import com.melink.online.service.UserService;
import com.melink.online.task.MessageTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
@Slf4j
@Transactional
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    UserRoleMapper userRoleMapper;
    @Autowired
    RoleMapper roleMapper;
    @Autowired
    PermissionRoleMapper permissionRoleMapper;
    @Autowired
    PermissionMapper permissionMapper;
    @Autowired
    MessageTask messageTask;
    @Autowired
    DeptMapper deptMapper;

    @Value("${wx.app-id}")
    private String appId;

    @Value("${wx.app-secret}")
    private String secret;

    /**
     * 通过用户ID查询用户并设置权限信息
     * @param id
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        if (null == id || id.isEmpty()) {
            throw new UsernameNotFoundException("用户名不能为空");
        }
        User user = userMapper.selectById(id);
        if (null == user) {
            throw new UsernameNotFoundException("用户不存在");
        }
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, user.getId()));
        if (userRoles != null && !userRoles.isEmpty()) {
            List<Integer> roleIds = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
            List<Role> roles = roleMapper.selectList(new QueryWrapper<Role>().lambda().in(Role::getId, roleIds));
            user.setRoleList(roles);
        }
        return user;
    }

    /**
     * 通过OpenId获取用户信息
     * @param openid
     * @return
     */
    @Override
    public User getUserByOpenId(String openid){
        User user = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getOpenId, openid));
        if (user == null){
            return null;
            //throw new MelinkException("不存在该用户");
        }
        return user;
    }

    /**
     * 获取角色权限信息
     * @param user
     * @return
     */
    @Override
    public List<Role> getAuthorities(User user) {
        if (user == null){
            return null;
            //throw new MelinkException("用户信息不存在，获取权限失败");
        }
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, user.getId()));
        if (userRoles == null){
            throw new MelinkException("不存在该用户角色");
        }
        List<Integer> roleIds = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
        List<Role> roles = roleMapper.selectList(new QueryWrapper<Role>().lambda().in(Role::getId, roleIds));
        if (roles == null){
            throw new MelinkException("权限信息获取失败");
        }
        return roles;
    }

    /**
     * 通过用户获取权限路径
     * @param user
     * @return
     */
    @Override
    public List<Permission> getPermAuthorities(User user) {
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, user.getId()));
        if (userRoles == null){
            throw new MelinkException("不存在该用户角色");
        }
        List<Integer> roleIds = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
        List<PermissionRole> permissionRoles = permissionRoleMapper.selectList(new QueryWrapper<PermissionRole>().lambda().in(PermissionRole::getRid, roleIds));
        List<Integer> permissionIds = permissionRoles.stream().map(PermissionRole::getId).collect(Collectors.toList());
        List<Permission> permissions = permissionMapper.selectList(new QueryWrapper<Permission>().lambda().in(Permission::getId, permissionIds));
        if (permissions == null){
            throw new MelinkException("权限信息获取失败");
        }
        return permissions;
    }

    /**
     * 通过用户ID获取角色信息
     * @param id
     * @return
     */
    @Override
    public List<Role> getAuthoritiesById(String id) {
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, id));
        if (userRoles == null){
            throw new MelinkException("不存在该用户角色");
        }
        List<Integer> roleIds = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
        List<Role> roles = roleMapper.selectList(new QueryWrapper<Role>().lambda().in(Role::getId, roleIds));
        if (roles == null){
            throw new MelinkException("权限信息获取失败");
        }
        return roles;
    }

    /**
     * 通过用户ID获取权限路径
     * @param id
     * @return
     */
    @Override
    public Set<String> getPermAuthoritiesById(String id) {
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, id));
        if (userRoles == null){
            throw new MelinkException("不存在该用户角色");
        }
        List<Integer> roleIds = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
        List<PermissionRole> permissionRoles = permissionRoleMapper.selectList(new QueryWrapper<PermissionRole>().lambda().in(PermissionRole::getRid, roleIds));
        List<Integer> permissionIds = permissionRoles.stream().map(PermissionRole::getId).collect(Collectors.toList());
        Set<String> permissions = permissionMapper.selectList(new QueryWrapper<Permission>().lambda().in(Permission::getId, permissionIds)).stream().map(Permission::getPermissionName).collect(Collectors.toSet());
        if (permissions == null){
            throw new MelinkException("权限信息获取失败");
        }
        return permissions;
    }

    /**
     * 获取用户ID
     * @param code
     * @return
     */
    @Override
    public String loginUser(String code) {
        String openId = getOpenId(code);
        String userId = userMapper.selectUserId(openId);
        if (userId == null || userId.isEmpty()){
            throw new MelinkException("账户不存在");
        }
        // TODO 从消息队列中获取消息，转移到消息表中
        messageTask.receiveAsync(userId);
        return userId;
    }

    /**
     * 获取员工入职日期
     * @param userId
     * @return
     */
    @Override
    public String searchUserHiredate(String userId) {
        String hiredate = userMapper.searchUserHiredate(userId);
        return hiredate;
    }

    /**
     * 查询用户页面信息
     * @param userId
     * @return
     */
    @Override
    public Map searchUserSummary(String userId) {
        Map map = userMapper.searchUserSummary(userId);
        return map;
    }

    /**
     * 返回每个部门的用户列表
     * @param username
     * @return
     */
    @Override
    public List<Map<String, Object>> searchUserGroupByDept(String username) {
        List<Map<String, Object>> deptListMap = deptMapper.searchDeptMembers(username);
        List<Map<String, Object>> userListMap = userMapper.searchUserGroupByDept(username);
        for (Map<String, Object> deptMap : deptListMap) {
            long id = (long) deptMap.get("id");
            // 添加用户信息到List集合中，然后把List集合添加到部门Map中返回
            List array = new ArrayList();
            for (Map<String, Object> userMap : userListMap) {
                long deptId = (long) userMap.get("deptId");
                // 只有当用户信息的部门ID与部门信息的ID相等时添加到List中
                if (id == deptId){
                    array.add(userMap);
                }
            }
            deptMap.put("members",array);
        }
        return deptListMap;
    }

    /**
     * 根据ID数组查询用户
     * @param ids
     * @return
     */
    @Override
    public List<Map> searchMembers(String[] ids) {
        List<String> idlist = Arrays.asList(ids);
        System.out.println("=====查看数组=====");
        idlist.forEach(System.out::println);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("status", 1).in(idlist!=null,"id", idlist);
        List<User> users = userMapper.selectList(wrapper);
        List resultList = new ArrayList();
        for (User user : users) {
            Map map = new HashMap();
            map.put("id",user.getId());
            map.put("name", user.getName());
            map.put("photo", user.getPhoto());
            resultList.add(map);
        }
        return resultList;
    }

    /**
     * 通过电话号码查询出用户
     * @param username 电话号码
     * @return
     */
    @Override
    public User getUserByTel(String username){
        if (username == null || username.isEmpty()) {
            throw new UsernameNotFoundException("用户名不能为空");
        }
        User user = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getTel, username));
        if (user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        List<UserRole> userRoles = userRoleMapper.selectList(new QueryWrapper<UserRole>().lambda().eq(UserRole::getUid, user.getId()));
        if (null != userRoles && !userRoles.isEmpty()){
            List<Integer> collect = userRoles.stream().map(UserRole::getRid).collect(Collectors.toList());
            List<Role> roleList = roleMapper.selectList(new QueryWrapper<Role>().lambda().in(Role::getId, collect));
            user.setRoleList(roleList);
        }
        log.info("登录时查询用户信息的返回值:{}",user.toString());
        return user;
    }

    /**
     * 查询判断是否有超级管理员存在
     * @return
     */
    @Override
    public boolean havaRootUser(){
        Long aLong = userMapper.selectCount(new QueryWrapper<User>().lambda().eq(User::getRoot, 1));
        return aLong == 0 ? false : true;
    }

    /**
     * 添加管理员信息以及对应的角色信息
     * @param userDTO
     * @return
     */
    @Override
    public boolean addAdmin(UserDTO userDTO) {
        User user = new User();
        user.setOpenId(userDTO.getOpenId());
        user.setNickname(userDTO.getNickname());
        user.setPhoto(userDTO.getPhoto());
        user.setName(userDTO.getName());
        user.setSex(userDTO.getSex());
        user.setTel(userDTO.getTel());
        user.setEmail(userDTO.getEmail());
        user.setHiredate(userDTO.getHiredate());
        user.setRoot(1);
        user.setDeptId(userDTO.getDeptId());
        user.setStatus(1);
        user.setPassword(userDTO.getPassword());
        int insert = userMapper.insert(user);
        if (insert == 0){
            throw new MelinkException("添加用户失败，无法保存用户信息，请联系管理员");
        }
        return true;
    }

    /**
     * 获取小程序OpenId
     * @param code
     * @return
     */
    @Override
    public String getOpenId(String code) {
        String url = "https://api.weixin.qq.com/sns/jscode2session";
        Map<String,Object> map = new HashMap<>();
        map.put("appid",appId);
        map.put("secret",secret);
        map.put("js_code",code);
        map.put("grant_type", "authorization_code");
        String post = HttpUtil.post(url, map);
        JSONObject jsonObject = JSONUtil.parseObj(post);
        String openid = jsonObject.getStr("openid");
        if (null == openid || openid.length() == 0){
            throw new MelinkException("临时登录凭证错误");
        }
        return openid;
    }

    /**
     * 注册管理员账户，设置角色管理权限并返回用户ID
     * @param registerCode 注册邀请码（管理员注册默认950419）
     * @param code 小程序临时凭证
     * @param nikeName 微信昵称
     * @param photo 微信头像
     * @return
     */
    @Override
    public String registerAdmin(String registerCode, String code, String nikeName, String photo) {
        // 检查邀请码是否为默认邀请码
        if (!registerCode.equals("950419")){
            throw new MelinkException("邀请码不正确");
        }
        // 判断是否存在超级管理员
        boolean havaRoot = havaRootUser();
        if (havaRoot){
            throw new MelinkException("请勿重复添加超级管理员");
        }
        // 通过临时凭证获取微信OpenId
        String openId = getOpenId(code);
        // 插入管理员数据
        User user = new User();
        user.setOpenId(openId);
        user.setNickname(nikeName);
        user.setPhoto(photo);
        user.setRoot(1);
        user.setStatus(1);
        int inserUser = userMapper.insert(user);
        if (inserUser != 1){
            throw new MelinkException("用户信息添加失败");
        }
        // 通过OpenID查出用户ID
        String id = getUserIdByOpenId(openId);
        // 设置管理员权限为超级管理员
        UserRole userRole = new UserRole();
        userRole.setUid(id);
        userRole.setRid(0);
        int insertRole = userRoleMapper.insert(userRole);
        if (insertRole != 1){
            throw new MelinkException("用户角色添加失败");
        }

        // 发送消息通知用户注册成功
        MessageEntity message = new MessageEntity();
        message.setSenderId("0");
        message.setSenderName("系统消息");
        message.setUuid(IdUtil.simpleUUID());
        message.setMsg("您已注册为超级管理员，请及时补充您的个人信息");
        messageTask.sendAsync(id, message);
        // 返回用户ID
        return id;
    }

    /**
     * 通过openid查询用户id
     * @param openid
     * @return
     */
    @Override
    public String getUserIdByOpenId(String openid) {
        User user = userMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getOpenId, openid));
        String id = user.getId();
        return id;
    }

}
