package com.melink.online.service.impl;

import com.melink.online.entity.MessageEntity;
import com.melink.online.entity.MessageRefEntity;
import com.melink.online.mapper.MessageMapper;
import com.melink.online.mapper.MessageRefMapper;
import com.melink.online.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageMapper messageMapper;
    @Autowired
    MessageRefMapper messageRefMapper;

    @Override
    public String insertMessage(MessageEntity entity) {
        String insert = messageMapper.insert(entity);
        return insert;
    }

    @Override
    public List<Map> searchMessageByPage(String userId, long start, int length) {
        List<Map> maps = messageMapper.searchMessageByPage(userId, start, length);
        return maps;
    }

    @Override
    public Map searchMessageById(String id) {
        Map map = messageMapper.searchMessageById(id);
        return map;
    }

    @Override
    public String insertRef(MessageRefEntity entity) {
        String insert = messageRefMapper.insert(entity);
        return insert;
    }

    @Override
    public long searchUnreadCount(String userId) {
        long count = messageRefMapper.searchUnreadCount(userId);
        return count;
    }

    @Override
    public long searchLastCount(String userId) {
        long count = messageRefMapper.searchLastCount(userId);
        return count;
    }

    @Override
    public long updateUnreadMessage(String id) {
        long message = messageRefMapper.updateUndeadMessage(id);
        return message;
    }

    @Override
    public long deleteMessageRefById(String id) {
        return messageRefMapper.deleteMessageRefById(id);
    }

    @Override
    public long deleteUserMessageRef(String userId) {
        return messageRefMapper.deleteUserMessageRef(userId);
    }
}
