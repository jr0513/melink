package com.melink.online.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.melink.online.entity.City;
import com.melink.online.mapper.CityMapper;
import com.melink.online.service.CityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 疫情城市列表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements CityService {

    @Autowired
    CityMapper cityMapper;

    /**
     * 查询城市编码
     * @param city
     * @return 城市编码
     */
    @Override
    public String searchCode(String city) {
        String code = cityMapper.selectOne(new QueryWrapper<City>().lambda().eq(City::getCity, city)).getCode();
        return code;
    }
}
