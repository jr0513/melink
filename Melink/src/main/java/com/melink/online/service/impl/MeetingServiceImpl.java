package com.melink.online.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.melink.online.entity.Meeting;
import com.melink.online.entity.Member;
import com.melink.online.entity.User;
import com.melink.online.entity.form.SearchMyMeetingListByPageForm;
import com.melink.online.entity.vo.MeetingVO;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.MeetingMapper;
import com.melink.online.mapper.MemberMapper;
import com.melink.online.mapper.UserMapper;
import com.melink.online.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 会议表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@Service
@Transactional
public class MeetingServiceImpl extends ServiceImpl<MeetingMapper, Meeting> implements MeetingService {

    @Autowired
    MemberMapper memberMapper;

    @Autowired
    MeetingMapper meetingMapper;

    @Autowired
    UserMapper userMapper;

    /**
     * 创建会议
     * @param entity
     */
    @Override
    public void insertingMeeting(Meeting entity) {
        meetingMapper.insert(entity);
        Member member = new Member();
        member.setMid(entity.getId());
        member.setUid(entity.getCreatorId());
        int insert = memberMapper.insert(member);
        if (insert != 1){
            throw new MelinkException("会议创建失败");
        }
        // TODO 开启工作流审批
    }

    /**
     * 查询会议分页信息
     * @param form
     * @return
     */
    @Override
    public List<Map<String,Object>> searchMyMeetingListByPage(String userId, SearchMyMeetingListByPageForm form){
        // 查询用户信息
        LambdaQueryWrapper<User> userWrapper = new LambdaQueryWrapper<>();
        userWrapper.eq(User::getId,userId).eq(User::getStatus,1);
        User user = userMapper.selectOne(userWrapper);
        if (Objects.isNull(user)){
            throw new MelinkException("不存在该用户");
        }
        // 分页查询会议信息
        LambdaQueryWrapper<Meeting> meetingWrapper = new LambdaQueryWrapper<>();
        meetingWrapper.eq(Meeting::getCreatorId,user.getId()).orderByDesc(true,Meeting::getMeetingDate,Meeting::getStart,Meeting::getId);
        //List<Meeting> meetings = meetingMapper.selectList(meetingWrapper);
        Page<Meeting> page = new Page(form.getPage(), form.getLength());
        Page<Meeting> meetingPage = meetingMapper.selectPage(page, meetingWrapper);
        if (Objects.isNull(meetingPage)){
            throw new MelinkException("您没有创建的会议");
        }
        List<Meeting> records = meetingPage.getRecords();
        List<MeetingVO> meetingVOS = new ArrayList<>();
        for (Meeting meeting : records) {
            MeetingVO meetingVO = new MeetingVO();
            meetingVO.setId(meeting.getId());
            meetingVO.setUuid(meeting.getUuid());
            meetingVO.setTitle(meeting.getTitle());
            meetingVO.setName(user.getName());
            meetingVO.setMeetingDate(DateUtil.format(DateUtil.parse(meeting.getMeetingDate()),"yyyy年MM月dd日"));
            meetingVO.setPlace(meeting.getPlace());
            meetingVO.setStart(DateUtil.format(DateUtil.parse(meeting.getStart()),"HH:mm"));
            meetingVO.setEnd(DateUtil.format(DateUtil.parse(meeting.getEnd()),"HH:mm"));
            meetingVO.setType(meeting.getType());
            meetingVO.setDescribes(meeting.getDescribes());
            meetingVO.setStatus(meeting.getStatus());
            meetingVO.setPhoto(user.getPhoto());
            meetingVO.setHour(String.valueOf(DateUtil.between(DateUtil.parse(meeting.getStart()), DateUtil.parse(meeting.getEnd()), DateUnit.HOUR)));
            meetingVOS.add(meetingVO);
        }
        // 设置返回数据的格式(格式：[{[]}])：map和第二个数组都是需要多次创建
        // 判断日期标记
        String date = null;
        // 创建第一个数组
        List resultList = new ArrayList();
        // 先定义map，不赋值，因为map需要创建多个，否则添加数据时就会覆盖之前的数据
        Map resultMap = null;
        // 定义list，不赋值，因为需要构建不同是数组
        List array = null;
        for (MeetingVO vo : meetingVOS) {
            String temp = vo.getMeetingDate();
            if (!temp.equals(date)){
                date = temp;
                resultMap = new HashMap();
                resultMap.put("date",date);
                array = new ArrayList();
                resultMap.put("list",array);
                // 只有当日期不相同时才能添加到第一个数组中
                resultList.add(resultMap);
            }
            // 不管这次循环是否相同，都需要添加会议信息到array中，所以在判断外添加数据
            array.add(vo);
        }
        return resultList;
    }
}
