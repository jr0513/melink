package com.melink.online.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.melink.online.entity.Permission;
import com.melink.online.entity.Role;
import com.melink.online.entity.User;
import com.melink.online.entity.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface UserService extends IService<User>, UserDetailsService {

    User getUserByTel(String username);

    boolean havaRootUser();

    boolean addAdmin(UserDTO userDTO);

    String getOpenId(String code);

    String registerAdmin(String registerCode, String code, String nikeName, String photo);

    String getUserIdByOpenId(String openid);

    public User getUserByOpenId(String openid);

    public List<Role> getAuthorities(User user);

    public List<Permission> getPermAuthorities(User user);

    public List<Role> getAuthoritiesById(String id);

    public Set<String> getPermAuthoritiesById(String id);

    public String loginUser(String code);

    String searchUserHiredate(String userId);

    Map searchUserSummary(String userId);

    List<Map<String,Object>> searchUserGroupByDept(String username);

    List<Map> searchMembers(String[] ids);

}
