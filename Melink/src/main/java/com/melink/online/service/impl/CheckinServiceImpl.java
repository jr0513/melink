package com.melink.online.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateRange;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.melink.online.common.constant.SystemConstants;
import com.melink.online.common.utils.FaceEngineUtils;
import com.melink.online.entity.Checkin;
import com.melink.online.entity.dto.CheckinDTO;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.CheckinMapper;
import com.melink.online.mapper.HolidaysMapper;
import com.melink.online.mapper.UserMapper;
import com.melink.online.mapper.WorkdayMapper;
import com.melink.online.service.*;
import com.melink.online.task.EmailTask;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * <p>
 * 签到表 服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
@Scope("prototype")
public class CheckinServiceImpl extends ServiceImpl<CheckinMapper, Checkin> implements CheckinService {

    @Autowired
    CheckinMapper checkinMapper;
    @Autowired
    HolidaysService holidaysService;
    @Autowired
    HolidaysMapper holidaysMapper;
    @Autowired
    WorkdayService workdayService;
    @Autowired
    WorkdayMapper workdayMapper;
    @Autowired
    SystemConstants systemConstants;
    @Autowired
    FaceModelService faceModelService;
    @Autowired
    FaceEngineUtils faceEngineUtils;
    @Autowired
    CityService cityService;
    @Value("${melink.email.hr}")
    private String hrEmail;
    @Autowired
    EmailTask emailTask;
    @Autowired
    UserMapper userMapper;


    /**
     * 检查是否签到
     * @param userId 用户ID
     * @return
     */
    @Override
    public String validCanCheckin(String userId) {
        // 查看当前时间是否为工作日，工作日为true，非工作日为false
        boolean b1 = workdayService.searchTodayIsWorkday();
        // 查看当前时间是否为节假日，节假日为true，非节假日为false
        boolean b2 = holidaysService.searchTodayIsHoliday();
        String type = "工作日";
        if (DateUtil.date().isWeekend()){
            type = "休息日";
        }
        if (b1){
            type = "工作日";
        }else if (b2){
            type = "节假日";
        }
        if (type == "节假日"){
            return "节假日或休息日不需要考勤";
        }else {
            DateTime now = DateUtil.date();
            String startTime = DateUtil.today() + " " + systemConstants.getAttendanceStartTime();
            String endTime = DateUtil.today() + " " + systemConstants.getAttendanceEndTime();
            DateTime start = DateUtil.parse(startTime);
            DateTime end = DateUtil.parse(endTime);
            if (now.isBefore(start)){
                return "没有到上班考勤开始时间";
            }else if (now.isAfter(end)){
                return "超过上班考勤结束时间";
            }else {
                CheckinDTO checkinDTO = new CheckinDTO();
                checkinDTO.setUserId(userId);
                checkinDTO.setStartTime(startTime);
                checkinDTO.setEndTime(endTime);
                boolean checkinBoolean = haveCheckin(checkinDTO);
                if (checkinBoolean){
                    return "今天已经考勤，不用重复考勤";
                }else {
                    return "可以考勤";
                }
            }
        }
    }

    /**
     * 是否签到
     * @param checkinDTO
     * @return
     */
    @Override
    public boolean haveCheckin(CheckinDTO checkinDTO) {
        Checkin checkin = checkinMapper.selectOne(new LambdaQueryWrapper<Checkin>().eq(Checkin::getUserId, checkinDTO.getUserId()).between(Checkin::getCreateTime, checkinDTO.getStartTime(), checkinDTO.getEndTime()).last("limit 1"));
        if (checkin == null){
            return false;
        }
        return true;
    }

    /**
     * 添加签到信息
     * @param checkin
     */
    @Override
    public void insert(Checkin checkin) {
        int insert = checkinMapper.insert(checkin);
        if (insert != 1){
            throw new MelinkException("添加签到失败");
        }
    }

    /**
     * 签到
     * @param param
     */
    @Override
    public void checkin(Map param) {
        Date d1 = DateUtil.date();
        Date d2 = DateUtil.parse(DateUtil.today() + " " + systemConstants.getAttendanceTime());
        Date d3 = DateUtil.parse(DateUtil.today() + " " + systemConstants.getAttendanceEndTime());
        int status = 1;
        if (d1.compareTo(d2) <= 0){
            status = 1;
        }else if (d1.compareTo(d2) > 0 && d1.compareTo(d3)< 0){
            status = 2;
        }
        String userId = (String) param.get("userId");
        String faceModel = faceModelService.searchFaceModel(userId);
        if (faceModel == null || faceModel == ""){
            throw new MelinkException("不存在用户脸部模型");
        }else {
            float faceSimilar = faceEngineUtils.compareFace((String) param.get("path"), faceModel);
            if (faceSimilar <= 0.85){
                throw new MelinkException("非本人操作,签到无效");
            }else {
                int risk = 1;
                // 查询城市简称
                String city = (String) param.get("city");
                String district = (String) param.get("district");
                String address = (String) param.get("address");
                String country = (String) param.get("country");
                String province = (String) param.get("province");
                if (!StrUtil.isBlank(city) && !StrUtil.isBlank(district)){
                    String code = cityService.searchCode(city);
                    if (code == null){
                        throw new MelinkException("无法查询到该地区，请联系管理员");
                    }
                    String url = "http://m." + code + ".bendibao.com/news/yqdengji/?qu=" + district;
                    try {
                        Document document = Jsoup.connect(url).get();
                        Elements elements = document.getElementsByClass("list-content");
                        if (elements.size() > 0){
                            Element element = elements.get(0);
                            String text = element.select("p:last-child").text();
                            if ("高风险".equals(text)){
                                risk = 3;
                                // 发送邮件警告
                                Map<String, String> map = userMapper.searchNameAndDept(userId);
                                String name = map.get("name");
                                String deptName = map.get("dept_name");
                                deptName = deptName != null ? deptName : "";
                                SimpleMailMessage message = new SimpleMailMessage();
                                message.setTo(hrEmail);
                                message.setSubject("员工（" + name + "）身处高风险地区警告！");
                                message.setText(deptName + "员工（" + name + "），" + DateUtil.format(new Date(), "yyyy年MM月dd日") + "处于" + address + ",属于新冠疫情高风险地区，请及时与该员工联系，核实情况！");
                                emailTask.sendAsync(message);
                            }else if ("中风险".equals(text)){
                                risk = 2;
                                // 发送邮件警告
                                Map<String, String> map = userMapper.searchNameAndDept(userId);
                                String name = map.get("name");
                                String deptName = map.get("dept_name");
                                deptName = deptName != null ? deptName : "";
                                SimpleMailMessage message = new SimpleMailMessage();
                                message.setTo(hrEmail);
                                message.setSubject("员工（" + name + "）身处中风险地区警告！");
                                message.setText(deptName + "员工（" + name + "），" + DateUtil.format(new Date(), "yyyy年MM月dd日") + "处于" + address + ",属于新冠疫情中风险地区，请及时与该员工联系，核实情况！");
                                emailTask.sendAsync(message);
                            }else {
                                risk = 1;
                            }
                        }
                    } catch (IOException e) {
                        log.error("执行异常",e);
                        throw new MelinkException("获取风险等级失败");
                    }
                    // 保存签到记录
                }
                // 保存签到记录
                Checkin checkin = new Checkin();
                checkin.setUserId(userId);
                checkin.setAddress(address);
                checkin.setCountry(country);
                checkin.setProvince(province);
                checkin.setCity(city);
                checkin.setRisk(risk);
                checkin.setDistrict(district);
                checkin.setStatus(status);
                checkinMapper.insert(checkin);
            }
        }
    }

    /**
     * 查询当天签到信息
     * @param userId
     * @return
     */
    @Override
    public Map searchTodayCheckin(String userId) {
        Map map = checkinMapper.searchTodayCheckin(userId);
        return map;
    }

    /**
     * 查询用户签到了多少次
     * @param userId
     * @return
     */
    @Override
    public long searchCheckinDays(String userId) {
        long count = checkinMapper.searchCheckinDays(userId);
        return count;
    }

    /**
     * 查询一周内的签到结果
     * @param param
     * @return
     */
    @Override
    public List<Map> searchWeekCheckin(HashMap param) {
        List list = new ArrayList();
        // 在时间段内有哪些天签到
        List<Map> checkinList = checkinMapper.searchWeekCheckin(param);
        // 在这段时间内节假日有哪些天
        List<String> holidayList = holidaysMapper.searchHolidaysInRange(param);
        // 在这段时间内休息日有哪些天
        List<String> workdayList = workdayMapper.searchWorkdayInRange(param);
        // 获取开始日期和结束日期
        DateTime startDate = DateUtil.parseDate(param.get("startDate").toString());
        DateTime endDate = DateUtil.parseDate(param.get("endDate").toString());
        // 本周七天的日期对象
        DateRange range = DateUtil.range(startDate, endDate, DateField.DAY_OF_MONTH);
        range.forEach(one->{
            String date = one.toString("yyy-MM-dd");
            String type = "工作日";
            if (one.isWeekend()){
                type = "休息日";
            }
            if (holidayList != null && holidayList.contains(date)){
                type = "节假日";
            }else if (workdayList != null && workdayList.contains(date)){
                type = "工作日";
            }
            // 定义签到状态
            String status = "";
            if (type.equals("工作日") && DateUtil.compare(one,DateUtil.date()) <= 0){
                // 当小于今天时，设置为缺勤（默认）
                status = "缺勤";
                for (Map<String, String> map : checkinList) {
                    // 如果map中存在这天的考勤，则设置为map中的状态
                    if (map.containsValue(date)) {
                        status = map.get("status");
                        break;
                    }
                }
                DateTime endTime = DateUtil.parse(DateUtil.today()+" "+systemConstants.getAttendanceEndTime());
                String today = DateUtil.today();
                // 如果该日期是今天，并且现在时间还没有超过下班时间，则打卡状态设置为空
                if (date.equals(today) && DateUtil.date().isBefore(endTime)){
                    status = "";
                }
            }
            HashMap map = new HashMap();
            map.put("date",date);
            map.put("status",status);
            map.put("type",type);
            // 将星期一转换为周一
            map.put("day",one.dayOfWeekEnum().toChinese("周"));
            list.add(map);
        });
        return list;
    }

    /**
     * 查询一个月内的签到结果
     * @param param
     * @return
     */
    @Override
    public List<Map> searchMonthCheckin(HashMap param) {
        List<Map> maps = this.searchWeekCheckin(param);
        return maps;
    }


}
