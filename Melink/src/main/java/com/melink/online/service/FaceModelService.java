package com.melink.online.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.melink.online.entity.FaceModel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
public interface FaceModelService extends IService<FaceModel> {

    // 根据用户ID查询用户脸部数据
    String searchFaceModel(String userId);
    // 添加脸部数据
    boolean insertFaceModel(String userId, String faceModel);
    // 根据用户ID删除脸部数据
    int deleteFaceModel(String userId);

}
