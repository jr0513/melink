package com.melink.online.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.melink.online.entity.Holidays;
import com.melink.online.entity.Workday;
import com.melink.online.exception.MelinkException;
import com.melink.online.mapper.WorkdayMapper;
import com.melink.online.service.WorkdayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;
import java.time.LocalDate;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Service
public class WorkdayServiceImpl extends ServiceImpl<WorkdayMapper, Workday> implements WorkdayService {

    @Autowired
    WorkdayMapper workdayMapper;

    @Override
    public boolean searchTodayIsWorkday() {
        Workday workday = workdayMapper.selectOne(new QueryWrapper<Workday>().lambda().eq(Workday::getDate, LocalDate.now()).last("limit 1"));
        if (workday == null){
            return false;
        }
        return true;
    }
}
