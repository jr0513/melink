package com.melink.online.controller;

import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.form.DeleteMessageRefByIdForm;
import com.melink.online.entity.form.SearchMessageByIdForm;
import com.melink.online.entity.form.SearchMessageByPageForm;
import com.melink.online.entity.form.UpdateUnreadMessageForm;
import com.melink.online.service.MessageService;
import com.melink.online.service.UserService;
import com.melink.online.task.MessageTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api("消息模块网络接口")
@RequestMapping("/message")
public class MessageController {
    @Autowired
    MessageService messageService;
    @Autowired
    UserService userService;
    @Autowired
    MessageTask messageTask;

    @PostMapping("/searchMessageByPage")
    @ApiOperation("获取分页消息列表")
    public RespBean searchMessageByPage(@Valid @RequestBody SearchMessageByPageForm form){
        String openId = GetOpenIdFromSecurity.getOpenId();
        String userId = userService.getUserIdByOpenId(openId);
        Integer page = form.getPage();
        Integer length = form.getLength();
        long start = (page-1)*length;
        List<Map> maps = messageService.searchMessageByPage(userId, start, length);
        return RespBean.ok(maps);
    }

    @PostMapping("/searchMessageById")
    @ApiOperation("根据ID查询消息")
    public RespBean searchMessageById(@Valid @RequestBody SearchMessageByIdForm form){
        Map map = messageService.searchMessageById(form.getId());
        return RespBean.ok(map);
    }

    @PostMapping("/upadteUnreadMessage")
    @ApiOperation("未读消息改为已读消息")
    public RespBean upadteUnreadMessage(@Valid @RequestBody UpdateUnreadMessageForm form){
        long rows = messageService.updateUnreadMessage(form.getId());
        return RespBean.ok(rows == 1 ? true : false);
    }

    @PostMapping("/deleteMessageRefById")
    @ApiOperation("删除消息")
    public RespBean deleteMessageRefById(@Valid @RequestBody DeleteMessageRefByIdForm form){
        long rows = messageService.deleteMessageRefById(form.getId());
        return RespBean.ok(rows == 1 ? true : false);
    }

    @GetMapping("/refreshMessage")
    @ApiOperation("刷新消息")
    public RespBean refreshMessage(){
        String userId = userService.getUserIdByOpenId(GetOpenIdFromSecurity.getOpenId());
        messageTask.receiveAsync(userId);
        long lastRows = messageService.searchLastCount(userId);
        long unreadRows = messageService.searchUnreadCount(userId);
        Map map = new HashMap();
        map.put("lastRows",lastRows);
        map.put("unreadRows",unreadRows);
        return RespBean.ok(map);
    }
}
