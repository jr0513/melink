package com.melink.online.controller;


import cn.hutool.core.io.FileUtil;
import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.RespBean;
import com.melink.online.exception.MelinkException;
import com.melink.online.service.FaceModelService;
import com.melink.online.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@RestController
@RequestMapping("/face")
@Api("人脸模型控制器")
@Slf4j
public class FaceModelController {

    @Autowired
    UserService userService;
    @Value("${melink.image-folder}")
    private String imageFolder;
    @Autowired
    FaceModelService faceModelService;

    @PostMapping("/creatFaceMoldel")
    @ApiOperation("创建人脸模型")
    public RespBean create(@RequestParam("photo") MultipartFile file){
        if (file == null){
            return RespBean.fail("没有上传照片");
        }
        String fileName = file.getOriginalFilename().toLowerCase();
        if (!fileName.endsWith(".jpg")){
            return RespBean.fail("照片必须为JPG格式");
        }else {
            String path = imageFolder + "/" +fileName;
            String openId = GetOpenIdFromSecurity.getOpenId();
            String userId = userService.getUserIdByOpenId(openId);
            try {
                file.transferTo(Paths.get(path));
                faceModelService.insertFaceModel(userId,path);
                return RespBean.ok("人脸模型创建成功");
            } catch (IOException e) {
                log.error(e.getMessage(),e);
                throw new MelinkException("照片保存错误");
            }finally {
                FileUtil.del(path);
            }
        }
    }

}
