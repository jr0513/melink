package com.melink.online.controller;

import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RedisUtils;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.dto.RegisterDTO;
import com.melink.online.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
@Api("登录注册接口")
public class LoginController {

    @Autowired
    UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    RedisUtils redisUtils;

    @Value("${melink.jwt.cache-expire}")
    private int cacheExpire;

    @PostMapping("/register")
    @ApiOperation("注册接口")
    public RespBean register(@Valid @RequestBody RegisterDTO registerDTO){
        String registerCode = registerDTO.getRegisterCode();
        String code = registerDTO.getCode();
        String nickName = registerDTO.getNickName();
        String photo = registerDTO.getPhoto();
        // 注册用户并获取ID
        String id = userService.registerAdmin(registerCode, code, nickName, photo);
        // 通过ID创建token
        String token = jwtUtils.createToken(id);
        // 查询角色权限信息
        Set<String> authorities = userService.getPermAuthoritiesById(id);
        // redis中缓存token
        redisUtils.setExpireTokenOfDay(token, id, cacheExpire);
        Map<String, Object> map = new HashMap<>();
        map.put("token",token);
        map.put("permissions",authorities);
        return RespBean.ok("注册成功", map);
    }
}
