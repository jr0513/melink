package com.melink.online.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 节假日表 前端控制器
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Controller
@RequestMapping("/online/holidays-entity")
public class HolidaysController {

}
