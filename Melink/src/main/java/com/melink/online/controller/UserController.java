package com.melink.online.controller;


import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RedisUtils;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.User;
import com.melink.online.entity.dto.RegisterDTO;
import com.melink.online.entity.form.SearchMembersForm;
import com.melink.online.entity.form.SearchUserGroupByDeptForm;
import com.melink.online.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@RestController
@Api("用户信息控制器")
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/searchUserSummary")
    @ApiOperation("查询用户摘要信息")
    public RespBean searchUserSummary(){
        String openId = GetOpenIdFromSecurity.getOpenId();
        String userId = userService.getUserIdByOpenId(openId);
        Map map = userService.searchUserSummary(userId);
        return RespBean.ok("成功",map);
    }

    @PostMapping("/searchUserGroupByDept")
    @ApiOperation("查询员工列表，按部门排序")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_gm','ROLE_dm')")
    public RespBean searchUserGroupByDept(@Valid @RequestBody SearchUserGroupByDeptForm form){
        List<Map<String, Object>> maps = userService.searchUserGroupByDept(form.getUsername());
        return RespBean.ok(maps);
    }

    @PostMapping("/searchMembers")
    @ApiOperation("查询会议成员")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_gm','ROLE_dm')")
    public RespBean searchMembers(@Valid @RequestBody SearchMembersForm form){
        List<Map> maps = userService.searchMembers(form.getIds());
        return RespBean.ok(maps);
    }
}
