package com.melink.online.controller;


import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import com.melink.online.common.constant.SystemConstants;
import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.JwtUtils;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.form.CheckinForm;
import com.melink.online.entity.form.SearchMonthCheckinForm;
import com.melink.online.exception.MelinkException;
import com.melink.online.service.CheckinService;
import com.melink.online.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到表 前端控制器
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@RestController
@RequestMapping("/checkin")
@Api("签到模块接口")
@Slf4j
public class CheckinController {

    @Autowired
    CheckinService checkinService;
    @Autowired
    UserService userService;
    @Value("${melink.image-folder}")
    private String imageFolder;
    @Autowired
    SystemConstants constants;
    @Autowired
    JwtUtils jwtUtils;

    @GetMapping("/validCanCheckin")
    @ApiOperation("查询签到日期")
    public RespBean validCanCheckin(){
        String openId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        for (GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            System.out.println(authority.getAuthority());
        }
        String userId = userService.getUserIdByOpenId(openId);
        String result = checkinService.validCanCheckin(userId);
        return RespBean.ok(result);
    }

    @PostMapping("/checkin")
    @ApiOperation("签到")
    public RespBean checkin(@Valid CheckinForm form, @RequestParam("photo") MultipartFile file){
        if (file == null){
            return RespBean.fail("没有上传照片");
        }
        String fileName = file.getOriginalFilename().toLowerCase();
        if (!fileName.endsWith(".jpg")){
            return RespBean.fail("照片必须为JPG格式");
        }else {
            String path = imageFolder + "/" +fileName;
            String openId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            String userId = userService.getUserIdByOpenId(openId);
            try {
                file.transferTo(Paths.get(path));
                Map param = new HashMap();
                param.put("userId",userId);
                param.put("path",path);
                param.put("city",form.getCity());
                param.put("district",form.getDistrict());
                param.put("address",form.getAddress());
                param.put("country",form.getCountry());
                param.put("province",form.getProvince());
                checkinService.checkin(param);
                return RespBean.ok("签到成功");
            } catch (IOException e) {
                log.error(e.getMessage(),e);
                throw new MelinkException("照片保存错误");
            }finally {
                FileUtil.del(path);
            }
        }
    }

    @GetMapping("/searchTodayChekin")
    @ApiOperation("查询用户当日签到数据")
    public RespBean searchTodayCheckin(){
        String userId = userService.getUserIdByOpenId(GetOpenIdFromSecurity.getOpenId());
        Map map = checkinService.searchTodayCheckin(userId);
        map.put("attendanceTime",constants.getAttendanceTime());
        map.put("closingTime",constants.getClosingTime());
        long days = checkinService.searchCheckinDays(userId);
        map.put("checkinDays",days);

        DateTime hiredate = DateUtil.parse(userService.searchUserHiredate(userId));
        DateTime startDate = DateUtil.beginOfWeek(DateUtil.date());
        if (startDate.isBefore(hiredate)){
            startDate = hiredate;
        }
        DateTime endDate = DateUtil.endOfWeek(DateUtil.date());
        HashMap param = new HashMap();
        param.put("startDate",startDate.toString());
        param.put("endDate",endDate.toString());
        param.put("userId",userId);
        List<Map> list = checkinService.searchWeekCheckin(param);
        map.put("weekCheckin",list);
        String token = jwtUtils.createToken(userId);
        map.put("token",token);
        return RespBean.ok("查询成功",map);
    }

    @PostMapping("/searchMonthCheckin")
    @ApiOperation("查询用户月签到数据")
    public RespBean searchMonthCheckin(@Valid @RequestBody SearchMonthCheckinForm form){
        String userId = userService.getUserIdByOpenId(GetOpenIdFromSecurity.getOpenId());
        // 入职日期
        DateTime hiredate = DateUtil.parse(userService.searchUserHiredate(userId));
        // 在月份前添加0
        String month = form.getMonth() < 10 ? "0" + form.getMonth() : form.getMonth().toString();
        DateTime startDate = DateUtil.parse(form.getYear() + "-" + month + "-" + "01");
        if (startDate.isBefore(DateUtil.beginOfMonth(hiredate))){
            throw new MelinkException("不能查看入职以前的签到数据");
        }
        if(startDate.isBefore(hiredate)){
            // 当开始日期在入职之前，那就赋值入职日期为开始计算签到日
            startDate = hiredate;
        }
        // 当月结束日
        DateTime endDate = DateUtil.endOfMonth(startDate);
        HashMap map = new HashMap();
        map.put("userId",userId);
        map.put("startDate",startDate.toString());
        map.put("endDate",endDate.toString());
        List<Map> list = checkinService.searchMonthCheckin(map);
        int sum_1 = 0, sum_2 = 0, sum_3 = 0;
        for (Map<String, String> one : list) {
            String type = one.get("type");
            String status = one.get("status");
            if ("工作日".equals(type)){
                if ("正常".equals(status)){
                    sum_1++;
                }else if ("迟到".equals(status)){
                    sum_2++;
                }else if ("缺勤".equals(status)){
                    sum_3++;
                }
            }
        }
        Map data = new HashMap();
        data.put("list",list);
        data.put("sum_1",sum_1);
        data.put("sum_2",sum_2);
        data.put("sum_3",sum_3);
        return RespBean.ok(data);
    }
}
