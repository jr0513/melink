package com.melink.online.controller;


import com.melink.online.common.utils.GetOpenIdFromSecurity;
import com.melink.online.common.utils.RespBean;
import com.melink.online.entity.form.SearchMyMeetingListByPageForm;
import com.melink.online.entity.vo.MeetingVO;
import com.melink.online.service.MeetingService;
import com.melink.online.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 会议表 前端控制器
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@RestController
@RequestMapping("/meeting")
public class MeetingController {
    @Autowired
    private MeetingService meetingService;
    @Autowired
    private UserService userService;

    @PostMapping("/searchMyMeetingListByPage")
    @ApiOperation("查询会议列表分页数据")
    public RespBean searchMyMeetingListByPage(@Valid @RequestBody SearchMyMeetingListByPageForm form){
        String userId = userService.getUserIdByOpenId(GetOpenIdFromSecurity.getOpenId());
        List<Map<String,Object>> maps = meetingService.searchMyMeetingListByPage(userId, form);
        return RespBean.ok(maps);
    }
}
