package com.melink.online.controller;

import com.melink.online.common.utils.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("测试类")
public class TestController {

    @PostMapping("/test")
    public RespBean test(){
        return RespBean.ok("注册成功");
    }

    @PostMapping("/addUser")
    @ApiOperation("测试添加用户")
    @PreAuthorize("hasRole('ROLE_admin')")
    public RespBean addUser(){
        return RespBean.ok("添加成功");
    }

}
