package com.melink.online.common.utils;

import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 获取userId工具类
 */
public class GetOpenIdFromSecurity {
    public static String getOpenId(){
        String openId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return openId;
    }
}
