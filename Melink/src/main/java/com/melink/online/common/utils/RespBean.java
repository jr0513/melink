package com.melink.online.common.utils;

public class RespBean {
    private Integer status;
    private String msg;
    private Object data;

    public static RespBean ok(String msg){
        return new RespBean(200, msg, null);
    }

    public static RespBean ok(String msg, Object data){
        return new RespBean(200, msg, data);
    }

    public static RespBean ok(Object data){
        return new RespBean(200, "成功", data);
    }

    public static RespBean fail(String msg){
        return new RespBean(500, msg, null);
    }

    public static RespBean fail(String msg, Object data){
        return new RespBean(500, msg, data);
    }

    private RespBean(){}

    public RespBean(Integer status, String msg, Object data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
