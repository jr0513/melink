package com.melink.online.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {

    @Autowired
    RedisTemplate redisTemplate;

    //保存token信息并设置过期时间
    public void setExpireTokenOfDay(String token, String id, int expire){
        redisTemplate.opsForValue().set(id, token, expire, TimeUnit.DAYS);
    }

    public Object getStringValue(String key){
        Boolean aBoolean = redisTemplate.hasKey(key);
        if (aBoolean){
            return redisTemplate.opsForValue().get(key);
        }
        return null;
    }

}
