package com.melink.online.common.utils;

import com.arcsoft.face.*;
import com.arcsoft.face.enums.DetectMode;
import com.arcsoft.face.enums.DetectOrient;
import com.arcsoft.face.enums.ErrorInfo;
import com.arcsoft.face.toolkit.ImageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.arcsoft.face.toolkit.ImageFactory.getRGBData;

@Component
@Slf4j
public class FaceEngineUtils {

    //从官网获取
    @Value("${melink.arcsoft.app-id}")
    private String appId;
    @Value("${melink.arcsoft.sdk-key}")
    private String sdkKey;

    // 初始化人脸识别引擎
    public FaceEngine initEngine(){
        FaceEngine faceEngine = new FaceEngine("D:\\File\\arcsoft-lib");
        //激活引擎
        int errorCode = faceEngine.activeOnline(appId, sdkKey);

        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            log.error("引擎激活失败");
        }
        ActiveFileInfo activeFileInfo=new ActiveFileInfo();
        errorCode = faceEngine.getActiveFileInfo(activeFileInfo);
        if (errorCode != ErrorInfo.MOK.getValue() && errorCode != ErrorInfo.MERR_ASF_ALREADY_ACTIVATED.getValue()) {
            log.error("获取激活文件信息失败");
        }
        //引擎配置
        EngineConfiguration engineConfiguration = new EngineConfiguration();
        engineConfiguration.setDetectMode(DetectMode.ASF_DETECT_MODE_IMAGE);
        engineConfiguration.setDetectFaceOrientPriority(DetectOrient.ASF_OP_ALL_OUT);
        engineConfiguration.setDetectFaceMaxNum(10);
        engineConfiguration.setDetectFaceScaleVal(16);
        //功能配置
        FunctionConfiguration functionConfiguration = new FunctionConfiguration();
        functionConfiguration.setSupportAge(true);
        functionConfiguration.setSupportFace3dAngle(true);
        functionConfiguration.setSupportFaceDetect(true);
        functionConfiguration.setSupportFaceRecognition(true);
        functionConfiguration.setSupportGender(true);
        functionConfiguration.setSupportLiveness(true);
        functionConfiguration.setSupportIRLiveness(true);
        engineConfiguration.setFunctionConfiguration(functionConfiguration);
        //初始化引擎
        errorCode = faceEngine.init(engineConfiguration);
        if (errorCode != ErrorInfo.MOK.getValue()) {
            log.error("初始化引擎失败");
        }
        return faceEngine;
    }

    // 添加人脸，返回识别脸部数据（String类型）
    public String addFace(String imagePath){
        FaceEngine faceEngine = initEngine();
        // 错误码
        int code;
        //人脸检测
        ImageInfo imageInfo = getRGBData(new File(imagePath));
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        code = faceEngine.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList);
        if (code != 0){
            log.error("人脸检测错误码:{}",code);
        }
        //特征提取
        FaceFeature faceFeature = new FaceFeature();
        code = faceEngine.extractFaceFeature(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList.get(0), faceFeature);
        if (code != 0){
            log.error("人脸提取错误码:{}",code);
        }
        byte[] faceDataByte = faceFeature.getFeatureData();
        // byte转换为String
        BASE64Encoder encoder = new BASE64Encoder();
        String faceDataString = encoder.encodeBuffer(faceDataByte);
        //引擎卸载
        code = faceEngine.unInit();
        if (code != 0){
            log.error("引擎卸载错误码:{}",code);
        }
        return faceDataString;
    }

    // 人脸对比
    public float compareFace(String sourcePath, String faceDataString){
        FaceEngine faceEngine = initEngine();
        // 错误码
        int code;
        //人脸检测
        ImageInfo imageInfo = getRGBData(new File(sourcePath));
        List<FaceInfo> faceInfoList = new ArrayList<FaceInfo>();
        code = faceEngine.detectFaces(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(),imageInfo.getImageFormat(), faceInfoList);
        if (code != 0){
            log.error("人脸检测错误码:{}",code);
        }
        //特征提取
        FaceFeature faceFeature = new FaceFeature();
        code = faceEngine.extractFaceFeature(imageInfo.getImageData(), imageInfo.getWidth(), imageInfo.getHeight(), imageInfo.getImageFormat(), faceInfoList.get(0), faceFeature);
        if (code != 0){
            log.error("特征提取错误码:{}",code);
        }
        //特征比对
        FaceFeature targetFaceFeature = new FaceFeature();
        // String转为Byte
        BASE64Decoder decoder = new BASE64Decoder();
        byte[] image = null;
        try {
            image = decoder.decodeBuffer(faceDataString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        targetFaceFeature.setFeatureData(faceFeature.getFeatureData());
        FaceFeature sourceFaceFeature = new FaceFeature();
        sourceFaceFeature.setFeatureData(image);
        FaceSimilar faceSimilar = new FaceSimilar();
        code = faceEngine.compareFaceFeature(targetFaceFeature, sourceFaceFeature, faceSimilar);
        if (code != 0){
            log.error("特征比对错误码:{}",code);
        }
        //引擎卸载
        code = faceEngine.unInit();
        if (code != 0){
            log.error("引擎卸载错误码:{}",code);
        }
        return faceSimilar.getScore();
    }
}
