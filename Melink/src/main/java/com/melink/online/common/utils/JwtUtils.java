package com.melink.online.common.utils;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class JwtUtils {

    @Value("${melink.jwt.secret}")
    private String secret;

    @Value("${melink.jwt.expire}")
    private int expire;

    // 创建 token
    public String createToken(String userId){
        // 签名算法
        Algorithm algorithm = Algorithm.HMAC256(secret);
        // 过期时间
        Date dateTime = DateUtil.offset(new Date(), DateField.DAY_OF_YEAR, expire);
        JWTCreator.Builder builder = JWT.create();
        String token = builder.withClaim("userId", userId)
                .withExpiresAt(dateTime)
                .sign(algorithm);
        return token;
    }

    // 获取用户ID
    public String getUserId(String token){
        // 验证签名
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret)).build();
        DecodedJWT verify = verifier.verify(token);
        String userId = verify.getClaim("userId").asString();
        return userId;
    }
}

