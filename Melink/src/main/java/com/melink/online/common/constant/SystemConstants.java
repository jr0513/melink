package com.melink.online.common.constant;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class SystemConstants {

    private String attendanceStartTime;
    private String attendanceTime;
    private String attendanceEndTime;
    private String closingStartTime;
    private String closingTime;
    private String closingEndTime;

}
