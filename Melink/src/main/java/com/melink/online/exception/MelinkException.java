package com.melink.online.exception;

import lombok.Data;

@Data
public class MelinkException extends RuntimeException{

    private String msg;
    private int code = 500;

    public MelinkException(String msg){
        super(msg);
        this.msg = msg;
    }

    public MelinkException(String msg, int code){
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public MelinkException(String msg, Throwable e){
        super(msg,e);
        this.msg = msg;
    }

    public MelinkException(String msg, int code, Throwable e){
        super(msg,e);
        this.msg = msg;
        this.code = code;
    }
}
