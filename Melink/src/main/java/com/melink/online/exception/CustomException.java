package com.melink.online.exception;

import com.melink.online.common.utils.RespBean;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CustomException {

    @ExceptionHandler(MelinkException.class)
    public RespBean handle(MelinkException exception){
        return RespBean.fail(exception.getMsg());
    }

}
