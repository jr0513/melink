package com.melink.online.task;

import com.melink.online.entity.MessageEntity;
import com.melink.online.entity.MessageRefEntity;
import com.melink.online.exception.MelinkException;
import com.melink.online.service.MessageService;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class MessageTask {

    @Autowired
    private ConnectionFactory factory;
    @Autowired
    private MessageService messageService;

    /**
     * 同步发送消息
     * @param topic
     * @param entity
     */
    public void send(String topic, MessageEntity entity){
        // 获取在MongoDB中保存成功的消息ID
        String id = messageService.insertMessage(entity);
        // 创建消息队列连接
        try (Connection connection = factory.newConnection();
             // 创建消息队列通道
             Channel channel = connection.createChannel();
        ){
            // 创建消息队列
            channel.queueDeclare(topic, true, false, false, null);
            // 创建发送队列的参数（header）
            Map map = new HashMap();
            map.put("messageId",id);
            AMQP.BasicProperties properties = new AMQP.BasicProperties().builder().headers(map).build();
            // 发布消息到队列中
            channel.basicPublish("",topic,properties,entity.getMsg().getBytes());
            log.debug("RabbitMQ消息发送成功");
        }catch (Exception e){
            log.error("RabbitMQ消息发布失败",e);
            throw new MelinkException("RabbitMQ消息发布失败");
        }
    }

    /**
     * 异步发送消息
     * @param topic
     * @param entity
     */
    @Async
    public void sendAsync(String topic, MessageEntity entity){
        send(topic, entity);
    }

    /**
     * 同步接收消息
     * @param topic
     * @return
     */
    public Integer receive(String topic){
        int i = 0;
        try (Connection connection = factory.newConnection();
             // 创建消息队列通道
             Channel channel = connection.createChannel();
        ){
            // 创建消息队列
            channel.queueDeclare(topic, true, false, false, null);
            while (true){
                // 接受消息
                GetResponse response = channel.basicGet(topic, false);
                if (response != null){
                    // 获取消息头中的messageId
                    Map<String, Object> headers = response.getProps().getHeaders();
                    String messageId = headers.get("messageId").toString();
                    // 获取消息内容
                    String message = new String(response.getBody());
                    log.debug("从RabbitMQ接收的消息：",message);
                    // 设置MessageRef
                    MessageRefEntity messageRef = new MessageRefEntity();
                    messageRef.setMessageId(messageId);
                    messageRef.setReceiverId(topic);
                    messageRef.setReadFlag(false);
                    messageRef.setLastFlag(true);
                    // 存入MongoDB
                    messageService.insertRef(messageRef);
                    //设置手动消息应答
                    long deliveryTag = response.getEnvelope().getDeliveryTag();
                    channel.basicAck(deliveryTag,false);
                    i++;
                }else {
                    break;
                }
            }
        }catch (Exception e){
            log.error("RabbitMQ消息接收失败",e);
            throw new MelinkException("RabbitMQ消息接收失败");
        }
        return i;
    }

    /**
     * 异步接收消息
     * @param topic
     * @return
     */
    @Async
    public Integer receiveAsync(String topic){
        return receive(topic);
    }

    /**
     * 同步删除队列
     * @param topic
     */
    public void deleteQueue(String topic){
        try (Connection connection = factory.newConnection();
             // 创建消息队列通道
             Channel channel = connection.createChannel();
        ){
            channel.queueDelete(topic);
            log.debug("RabbitMQ消息队列删除成功");
        }catch (Exception e){
            log.error("RabbitMQ消息队列删除失败",e);
            throw new MelinkException("RabbitMQ消息队列删除失败");
        }
    }

    /**
     * 异步删除队列
     * @param topic
     */
    @Async
    public void deleteQueueAsync(String topic){
        deleteQueue(topic);
    }
}
