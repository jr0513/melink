package com.melink.online.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.melink.online.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    String selectUserId(String openId);

    HashMap searchNameAndDept(String userId);

    String searchUserHiredate(String userId);

    Map searchUserSummary(String userId);

    List<Map<String,Object>> searchUserGroupByDept(String username);
}
