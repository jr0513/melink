package com.melink.online.mapper;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;
import com.melink.online.entity.MessageEntity;
import com.melink.online.entity.MessageRefEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class MessageMapper {

    @Autowired
    MongoTemplate template;

    /**
     * 添加数据到MongoDB
     * @param entity
     * @return
     */
    public String insert(MessageEntity entity){
        Date sendTime = entity.getSendTime();
        sendTime = DateUtil.offset(sendTime, DateField.HOUR,8);
        entity.setSendTime(sendTime);
        entity = template.save(entity);
        return entity.get_id();
    }

    /**
     * 分页查询消息引用数据
     * @param userId
     * @param start
     * @param length
     * @return
     */
    public List<Map> searchMessageByPage(String userId, long start, int length){
        // 聚合操作（返回自定义修改过的数据）
        /*db.message.aggregate([
	        {
		        $set:{
			        "id":{ $toString: "$_id"}
		        }
	        },
	        {
		        $lookup:{
			        from: "message_ref",
			        localField: "id",
			        foreignField: "messageId",
			        as: "ref"
		        },
	        },
	        {$match:{"ref.receiverId":"85b6964dec6ae5d386f5ff320404517a"}},
	        {$sort:{sendTime:-1}},
	        {$skip:0},
	        {$limit:50}
        ])*/
        JSONObject json = new JSONObject();
        json.set("$toString","$_id");
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.addFields().addField("id").withValue(json).build(),
                Aggregation.lookup("message_ref","id","messageId","ref"),
                Aggregation.match(Criteria.where("ref.receiverId").is(userId)),
                Aggregation.sort(Sort.by(Sort.Direction.DESC,"sendTime")),
                Aggregation.skip(start),
                Aggregation.limit(length)
        );
        AggregationResults<Map> results = template.aggregate(aggregation, "message", Map.class);
        List<Map> list = results.getMappedResults();
        list.forEach(one->{
            // 把两个表整合数据，设置需要的数据，再删除不要的。
            List<MessageRefEntity> refList = (List<MessageRefEntity>) one.get("ref");
            MessageRefEntity entity = refList.get(0);
            Boolean readFlag = entity.getReadFlag();
            String refId = entity.get_id();
            one.put("readFlag",readFlag);
            one.put("refId",refId);
            one.remove("ref");
            one.remove("_id");
            Date sendTime = (Date) one.get("sendTime");
            sendTime = DateUtil.offset(sendTime, DateField.HOUR, -8);
            String today = DateUtil.today();
            if (today.equals(DateUtil.date(sendTime).toDateStr())){
                one.put("sendTime",DateUtil.format(sendTime,"HH:mm"));
            }else {
                one.put("sendTime",DateUtil.format(sendTime,"yyyy/MM/dd"));
            }
        });
        return list;
    }

    /**
     * 通过Id查询信息
     * @param id
     * @return
     */
    public Map searchMessageById(String id){
        Map map = template.findById(id, Map.class, "message");
        Date sendTime = (Date) map.get("sendTime");
        sendTime = DateUtil.offset(sendTime,DateField.HOUR,-8);
        map.replace("sendTime",DateUtil.format(sendTime,"yyyy-MM-dd HH:mm"));
        return map;
    }

}
