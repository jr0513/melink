package com.melink.online.mapper;

import com.melink.online.entity.Dept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface DeptMapper extends BaseMapper<Dept> {

    List<Map<String,Object>> searchDeptMembers(String username);
}
