package com.melink.online.mapper;

import com.melink.online.entity.Holidays;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 节假日表 Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface HolidaysMapper extends BaseMapper<Holidays> {
    List<String> searchHolidaysInRange(Map param);
}
