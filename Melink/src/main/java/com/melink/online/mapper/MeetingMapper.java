package com.melink.online.mapper;

import com.melink.online.entity.Meeting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 会议表 Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@Mapper
public interface MeetingMapper extends BaseMapper<Meeting> {

}
