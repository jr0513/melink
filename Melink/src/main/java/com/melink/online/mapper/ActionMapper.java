package com.melink.online.mapper;

import com.melink.online.entity.Action;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 行为表 Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface ActionMapper extends BaseMapper<Action> {

}
