package com.melink.online.mapper;

import com.melink.online.entity.PermissionRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2022-01-18
 */
@Mapper
public interface PermissionRoleMapper extends BaseMapper<PermissionRole> {

}
