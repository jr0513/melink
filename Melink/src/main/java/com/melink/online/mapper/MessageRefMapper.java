package com.melink.online.mapper;

import com.melink.online.entity.MessageRefEntity;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class MessageRefMapper {

    @Autowired
    MongoTemplate template;

    /**
     * 插入消息引用数据
     * @param entity
     * @return
     */
    public String insert(MessageRefEntity entity){
        entity = template.save(entity);
        return entity.get_id();
    }

    /**
     * 根据用户Id查询未读消息
     * @param userId
     * @return
     */
    public long searchUnreadCount(String userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("readFlag").is(false).and("receiverId").is(userId));
        long count = template.count(query, "message_ref");
        return count;
    }

    /**
     * 根据用户Id查询最新消息
     * @param userId
     * @return
     */
    public long searchLastCount(String userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("lastFlag").is(true).and("receiverId").is(userId));
        Update update = new Update();
        update.set("lastFlag",false);
        UpdateResult result = template.updateMulti(query, update, "message_ref");
        return result.getModifiedCount();
    }

    /**
     * 根据ID跟新未读消息为已读消息
     * @param id
     * @return
     */
    public long updateUndeadMessage(String id){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Update update = new Update();
        update.set("readFlag",true);
        UpdateResult result = template.updateFirst(query, update, "message_ref");
        return result.getModifiedCount();
    }

    /**
     * 根据Id删除消息引用
     * @param id
     * @return
     */
    public long deleteMessageRefById(String id){
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        DeleteResult result = template.remove(query, "message_ref");
        return result.getDeletedCount();
    }

    /**
     * 根据用户Id删除与用户相关的消息引用
     * @param userId
     * @return
     */
    public long deleteUserMessageRef(String userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("receiverId").is(userId));
        DeleteResult result = template.remove(query, "message_ref");
        return result.getDeletedCount();
    }
}
