package com.melink.online.mapper;

import com.melink.online.entity.Checkin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 签到表 Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface CheckinMapper extends BaseMapper<Checkin> {

    Map searchTodayCheckin (String userId);

    long searchCheckinDays (String userId);

    List<Map> searchWeekCheckin (HashMap param);

}
