package com.melink.online.mapper;

import com.melink.online.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2022-02-18
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

}
