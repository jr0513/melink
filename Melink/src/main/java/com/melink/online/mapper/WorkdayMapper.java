package com.melink.online.mapper;

import com.melink.online.entity.Workday;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 周迪
 * @since 2021-05-17
 */
@Mapper
public interface WorkdayMapper extends BaseMapper<Workday> {
    List<String> searchWorkdayInRange(Map param);
}
