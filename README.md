# Melink

#### 介绍 
该项目是个简单OA项目，采用小程序作为前端，SpringBoot单体构建服务端。主要的功能有微信登录、人脸签到、消息发送基础功能。

#### 软件架构
前端采用uni-app构建小程序项目，使用Vue编写前端。后端采用SpringBoot单体架构，SpringSecurity作为认证授权架构、使用JWT作为登录唯一认证码、并采用redis缓存token。人脸识别使用的是虹软，使用rabbitMQ实现发送消息并解耦，消息数据采用MongoDB存储，其他数据使用MySQL存储。

